"use strict";
const config = require('./config/config');
const winston = require("winston");
const fs = require("fs");
const path = require("path");
const Promise = require("bluebird");
const getAccounts = require('./automations/get_accounts');

const logDir = path.join(__dirname, 'logs');
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
    winston.info(`Created the log folder ${logDir}`);
}
winston.add(winston.transports.File, {filename: path.join(logDir, 'automations.log')});

winston.info(`Running automations`);


const commandLineParams = {};
process.argv.forEach((val) => {
    const param = val.split('=');
    commandLineParams[param[0]] = param[1];
});
if (commandLineParams.hasOwnProperty('--help')) {
    console.log(`Parameters examples:
    automation_type="demand_geo"
    account_ids=2
    platform_name="springserve"`);
    return;
}

let platformName;
if (commandLineParams.platform_name) {
    platformName = commandLineParams.platform_name;
}

let automationType;
if (commandLineParams.automation_type) {
    automationType = commandLineParams.automation_type;
}

let accountIds;
if (commandLineParams.account_id) {
    accountIds = commandLineParams.account_ids.split(',');
}

// automationType = 'demand_geo';
// platformName = 'springserve';
getAccounts(platformName, accountIds)
    .then(()=>{
        const accountKeys = Object.keys(config.accounts);
        winston.info(`Starting automation ${automationType} for ${platformName} on ${accountKeys.length} accounts`);

        if (platformName) {
            const automation = automationStartingPoints[platformName][automationType];
            return automation(config.accounts[accountKeys[0]]);
        }
        const automation = automationStartingPoints[automationType];
        return automation(config.accounts);
    })
    .catch(err=>{
        winston.error(err);
        throw err;
    });

const automationStartingPoints = {
    springserve: {
        demand_geo:     require(`./automations/springserve/demand_geo_automation`),
        demand_domain:  require(`./automations/springserve/demand_domain_automation`),
        supply_domain:  require(`./automations/springserve/supply_domain_automation`),
        supply_wl:      require(`./automations/springserve/supply_wl_automation`),
        // partner_mail:   require(`./automations/springserve/partner_mail_automation`),
    },
    streamrail: {
        demand_geo:     require(`./automations/streamrail/demand_geo_automation`),
        demand_domain:  require(`./automations/streamrail/demand_domain_automation`),
        supply_domain:  require(`./automations/streamrail/supply_domain_automation`),
        supply_wl:      require(`./automations/streamrail/supply_wl_automation`),
        // partner_mail:   require(`./automations/streamrail/partner_mail_automation`),
    },
    mail_follow_up: require(`./automations/mail_follow_up_automation`),
    wl_mail_automation: require(`./automations/wl_mail_automation`),
};
