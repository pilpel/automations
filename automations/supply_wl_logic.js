const config = require('./../config/config');
const winston = require("winston");
const utils = require('./utils');
const _ = require('lodash');

const filtersFieldMap = {
    tag_name: 'supply_tag_name',
    tag_id: 'supply_tag_id',
    partner: 'partner_name',
    env: 'env'
};

const geosToIgnore = ['COC', 'CEU', 'CSCA', 'CAF', 'CAS', 'CAN', '--'];
const numOfPartnersToKeep = 2;
const numOfTagsToOpen = 2;

function calculateFields(name, rows) {
    const obj = {
        impressions: _.sumBy(rows, row => row.impressions),
        requests: _.sumBy(rows, row => row.requests),
        profit: _.sumBy(rows, row => row.profit),
        revenue: _.sumBy(rows, row => row.revenue),
        opportunities: _.sumBy(rows, row => row.opportunities),
        name,
    };
    obj.opsFill = obj.opportunities && obj.impressions / obj.opportunities;
    obj.profitMargin = obj.revenue && obj.profit / obj.revenue;
    obj.rpm = obj.impressions && (obj.revenue / obj.impressions) * 1000;
    obj.roundedRpm = Math.round(obj.rpm / 0.25) * 0.25;
    return obj;
}

function getBestTwoPartners(rowsByPartner) {
    const sorted = _.sortBy(Object.keys(rowsByPartner), [function(partnerName) {
        return _.sumBy(rowsByPartner[partnerName], row => row.revenue);
    }]);
    return _.pick(rowsByPartner, sorted.slice(-numOfPartnersToKeep))
}

function logic(rows, allSupplies, filters, partnersNotToOpen) {

    const wlTags = {};

    rows = utils.applyFilters(rows, filters, filtersFieldMap);

    let rowsByPartner = _.groupBy(rows, 'partner_name');
    rowsByPartner = _.omit(rowsByPartner, partnersNotToOpen);
    rowsByPartner = getBestTwoPartners(rowsByPartner);
    const tagsMarkedForWl = {};
    const settings = config.automationSettings;

    function checkWhitelistConditions(geosToIgnore, countryCode, geoData, supplyData, topThreeGeosByRevenue) {
        if (geosToIgnore.includes(countryCode)) {
            return true;
        }

        if (geoData.impressions < supplyData.sums.impressions * settings.geo_imps_percent || geoData.revenue < settings.geo_revenue) {
            return true;
        }

        return !(geoData.opsFill >= supplyData.sums.opsFill ||
            (topThreeGeosByRevenue.includes(countryCode) && geoData.opsFill >= supplyData.sums.opsFill * settings.percent_of_opsfill_top_geos && geoData.opsFill > settings.min_ops_fill_top_geos));
    }

    function setWhitelistsRates() {
        _.values(wlTags).forEach(o => o.price = Math.round(Math.min(...o.prices) * 100) / 100)
    }

    function markedTagsHandler(supplyData, supplyName) {
        const rowsByCountry = _.groupBy(supplyData.rows, 'country_code');

        const geoSums = {};
        _.forOwn(rowsByCountry, (rowsOfCountry, countryCode) => {
            geoSums[countryCode] = calculateFields(countryCode, rowsOfCountry);
        });
        const topThreeGeosByRevenue = _.sortBy(Object.values(geoSums), ['revenue']).slice(-3).map(c => c.name);

        _.forOwn(rowsByCountry, (rowsOfCountry, countryCode) => {
            const geoData = geoSums[countryCode];

            if (checkWhitelistConditions(geosToIgnore, countryCode, geoData, supplyData, topThreeGeosByRevenue)) { return }

            const markedDomains = [];
            const markedBundles = [];
            rowsOfCountry.forEach(row => {
                if (row.impressions / row.opportunities < geoData.opsFill || row.impressions < geoData.impressions * settings.domain_imps_percent || row.declared_domain === 'unclassified') {
                    return;
                }
                (['unknown', ''].includes(row.declared_domain)) ? markedBundles.push(row.app_bundle) : markedDomains.push(row.declared_domain);
            });

            wlTags[supplyName] = wlTags[supplyName] || {
                prices: [],
                countries: [],
                partner: supplyData.partner,
                supplyId: supplyData.id,
                supplyName
            };
            if (geoData.profitMargin < settings.low_margin_upper_bound) {
                wlTags[supplyName].margin = 'low';
                wlTags[supplyName].prices.push(geoData.rpm - settings.low_margin_price_modifier * geoData.rpm);
            }
            else if (geoData.profitMargin <= settings.medium_margin_upper_bound) {
                wlTags[supplyName].margin = 'medium';
                wlTags[supplyName].prices.push(Number(allSupplies[supplyData.id].rate) + utils.getRandomArbitrary(settings.medium_margin_addition_min, settings.medium_margin_addition_max));
            }
            else {
                wlTags[supplyName].margin = 'high';
                wlTags[supplyName].prices.push(geoData.rpm - settings.high_margin_price_modifier * geoData.rpm);
            }
            wlTags[supplyName].countries.push(countryCode);
            if (markedDomains.length > 0) {
                wlTags[supplyName].domains = _.union(wlTags[supplyName].domains, markedDomains);
            }
            if (markedBundles.length > 0) {
                wlTags[supplyName].app_bundles = _.union(wlTags[supplyName].app_bundles, markedBundles);
            }
        });
    }

    function markBestTags(supplySums, partnerRowsBySupply, partner) {
        ['revenue'].forEach(highType => {
            const sorted = _.sortBy(Object.values(supplySums), [highType]);
            sorted.slice(-numOfTagsToOpen).forEach(sum => {
                tagsMarkedForWl[sum.name] = {
                    sums: sum,
                    rows: partnerRowsBySupply[sum.name],
                    id: partnerRowsBySupply[sum.name][0].supply_tag_id,
                    partner
                }
            })
        });
    }

    _.forOwn(rowsByPartner, (rowsOfPartner, partner) => {

        const supplySums = {};
        const partnerRowsBySupply = _.groupBy(rowsOfPartner, 'supply_tag_name');

        _.forOwn(partnerRowsBySupply, (rowsOfSupply, supply) => {
            supplySums[supply] = calculateFields(supply, rowsOfSupply);
        });

        markBestTags(supplySums, partnerRowsBySupply, partner);
    });

    _.forOwn(tagsMarkedForWl, markedTagsHandler);

    setWhitelistsRates();

    return removeSimilarWhitelists(wlTags);
}

function tagsAreSimilar(tag1, tag2) {
    if (tag1.partner !== tag2.partner) {
        return false
    }
    if (!_.isEqual(tag1.countries, tag2.countries)) {
        return false
    }
    if (tag1.domains && tag2.domains && !utils.isSubset(tag1.domains, tag2.domains)) {
        return false
    }
    if (tag1.app_bundles && tag2.app_bundles && !utils.isSubset(tag1.app_bundles, tag2.app_bundles)) {
        return false
    }
    const diff = tag1.price - tag2.price;
    if (diff < 0 || diff > 0.05) {
        return false
    }
    return true;
}

function removeSimilarWhitelists(wlTags) {
    _.forOwn(wlTags, (tagData, supplyName)=>{
        _.forOwn(wlTags, (otherTagData, otherSupplyName)=>{
            if (supplyName === otherSupplyName) {
                return
            }
            if (!tagsAreSimilar(tagData, otherTagData)) {
                return
            }
            delete wlTags[supplyName];
        });
    });
    return wlTags
}

module.exports = logic;