const StreamrailApi = require('./api');
const winston = require("winston");
const getSettings = require('../get_settings');
const Promise = require('bluebird');
const _ = require('lodash');
const util = require('util');
const utils = require('../utils');
const supplyWlLogic = require('./../supply_wl_logic');
const filterExistingWhitelists = require('./../whitelist_similarity').filterExistingWhitelists;
const mapObjectFields = require('./supply_wl_mapping');

const automationType = 'supply_wl';
const platform = 'streamrail';
const concurrency = 1;

function keyByAndRenameTrafficChannelField(allTrafficChannels) {
    const trafficChannelsObj = _.keyBy(allTrafficChannels, 'id');

    return _.mapValues(trafficChannelsObj, (trafficChannel) => {
        trafficChannel.rate = trafficChannel.cost;
        return trafficChannel
    });
}

function startAutomation(account) {
    const streamrailApi = new StreamrailApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        streamrailApi.login()
    ]).then(([, token]) => {
        winston.info(`Token acquired: ${token}`);
        return Promise.all([
            streamrailApi.getSupplyWlReport(),
            streamrailApi.getTrafficChannels(),
            utils.getFiltering(automationType, account.id),
            utils.getAlreadyOpenedWlPartners(account.id),
        ])
    })
        .then(async ([report, allTrafficChannels, filters, partnersNotToOpen]) => {
            const mappedRows = mapObjectFields(report);
            const trafficChannelsObj = keyByAndRenameTrafficChannelField(allTrafficChannels.data.trafficChannels);

            const whitelists = supplyWlLogic(mappedRows, trafficChannelsObj, filters, partnersNotToOpen);
            const newWhitelists = await filterExistingWhitelists(streamrailApi, whitelists, trafficChannelsObj, _.groupBy(mappedRows, 'supply_tag_id'));

            const promiseProducer = function () {
                if (_.isEmpty(newWhitelists)) {
                    return null;
                }

                const originalTagName = Object.keys(newWhitelists)[0];
                winston.info(`Starting WL promise chain for ${originalTagName}`);
                const wlData = newWhitelists[originalTagName];
                delete newWhitelists[originalTagName];

                winston.info(`Creating duplicate of supplyId ${wlData.supplyId}. price: ${wlData.price}. countries: ${wlData.countries}. domains: ${wlData.domains}. app_bundles: ${wlData.app_bundles}`);
                return streamrailApi.createSupplyWhitelistTag(wlData.supplyId, wlData.partner, wlData.price, wlData.countries, wlData.domains, wlData.app_bundles)
                    .then((newTag)=>{
                        newTag = newTag.data.trafficChannel;
                        return utils.storeWl(account.id, platform, wlData.partner, wlData.supplyId, newTag.id, newTag.name, newTag.cost, wlData.countries, wlData.domains, wlData.app_bundles, wlData.margin, newTag.env);
                    })
                    .catch(err=>{
                        winston.error(`Error handling supplyId ${wlData.supplyId}: ${util.inspect(err)}`);
                    })
            };

            return utils.fulfillPromisesSync(promiseProducer, concurrency)

        })
        .then(res => {
            winston.info(`Streamrail supply wl automation finished`)
        })
        .catch(err => {
            winston.info(err);
        });
}

module.exports = startAutomation;