const _ = require('lodash');
const axios = require("axios");
const winston = require('winston');
const Promise = require('bluebird');
const util = require('util');
const rp = require('request-promise');
const Readable = require('stream').Readable;

module.exports = ListsApi;

const automatedListPrefix = 'Automated list ';
const blackListRel = 'neq';
const whiteListRel = 'eq';

function ListsApi(api, listType) {
    this.listType = listType;
    this.api = api;

    this.rowKey = propertyMap[listType].rowKey;
    this.urlString = propertyMap[listType].urlString;
    this.jsonKey = propertyMap[listType].jsonKey;
    this.alternativeJsonKey = propertyMap[listType].alternativeJsonKey;
    this.possibleFilters = [this.jsonKey, this.alternativeJsonKey];
    this.txtUploadUrl = propertyMap[listType].txtUploadUrl;
    this.listNameExistsError = propertyMap[listType].listNameExistsError;
}

const propertyMap = {
    domain: {
        rowKey: 'domain',
        urlString: 'domain-lists',
        jsonKey: 'domainList',
        alternativeJsonKey: 'domainListInclude',
        txtUploadUrl: 'domainlist',
        listNameExistsError: 'DomainList of type domain with the same name already exist',
    },
    app: {
        rowKey: 'bundleId',
        urlString: 'app-lists',
        jsonKey: 'appList',
        alternativeJsonKey: 'appListInclude',
        txtUploadUrl: 'applist',
        listNameExistsError: 'DomainList of type apps with the same name already exist',
    },
    country: {
        rowKey: 'country',
        jsonKey: 'country',
    },
};

const AD_SOURCES = 'ad-sources';
const TRAFFIC_CHANNELS = 'traffic-channels';
const tagTypeMap = {};
tagTypeMap[AD_SOURCES] = 'adSource';
tagTypeMap[TRAFFIC_CHANNELS] = 'trafficChannel';

const actions = {
    block: {
        relTypeAdd: blackListRel,
        relTypeAddName : 'blacklist',
        relTypeRemove: whiteListRel,
        relTypeRemoveName : 'whitelist',
        name: 'block'
    },
    unblock: {
        relTypeAdd: whiteListRel,
        relTypeAddName : 'whitelist',
        relTypeRemove: blackListRel,
        relTypeRemoveName : 'blacklist',
        name: 'unblock'
    }
};


ListsApi.prototype.txtFieldBlock = function (tagId, items) {
    return this.txtFieldAction(tagId, items, actions.block)
};

ListsApi.prototype.txtFieldUnblock = function (tagId, items) {
    return this.txtFieldAction(tagId, items, actions.unblock)
};

ListsApi.prototype.txtFieldAction = function (tagId, items, action) {
    /**
     * Should only be used for list types that have few items such as country targeting
     * If used with domains / app bundles the UI will break
     */
    items = items.filter(e => e !== '');
    if (items.length === 0) {
        return Promise.resolve();
    }
    winston.info(`(${tagId}) Trying to ${action.name} ${items.length} ${this.listType}s`);
    return this.api.getAdSource(tagId)
        .then(res => {
            let targetingConditions = this.parseTargetingConditions(res.data[tagTypeMap[AD_SOURCES]].targetingConditions);
            const filterObj = targetingConditions[0].filters.find((f) => f.key === this.jsonKey);

            if (filterObj && filterObj.rel === action.relTypeAdd) {
                winston.info(`(${tagId}) ${this.listType} ${action.relTypeAddName} filter found`);
                filterObj.values = _.union(filterObj.values, items);
                return this.api.updateAdSource(tagId, generateParamsFromTargetingConditions(AD_SOURCES, targetingConditions));
            }
            if (filterObj && filterObj.rel === action.relTypeRemove) {
                winston.info(`(${tagId}) ${this.listType} ${action.relTypeRemoveName} filter found`);
                _.remove(filterObj.values, (item => items.includes(item)));
                if (filterObj.values.length === 0) {
                    winston.error(`Trying to remove all items from tag id ${tagId}`);
                    return;
                }
                return this.api.updateAdSource(tagId, generateParamsFromTargetingConditions(AD_SOURCES, targetingConditions));
            }

            if (action === actions.block) {
                winston.info(`(${tagId}) No ${this.listType} filter found. Creating a new one`);
                return this.createAndAttachBlackListFilter(targetingConditions, items, tagId, AD_SOURCES)
            }
            winston.info(`(${tagId}) No ${this.listType} filter found. Can't unblock`);
        })
};

ListsApi.prototype.createAndAttachBlackListFilter = function (targetingConditions, entities, tagId, tagType) {
    targetingConditions[0].filters.push({
        key: this.rowKey,
        rel: blackListRel,
        values: entities,
    });
    return this.api.updateAdSource(tagId, generateParamsFromTargetingConditions(tagType, targetingConditions));
};

function generateParamsFromTargetingConditions(tagType, targetingConditions) {
    const payload = {};
    payload[tagTypeMap[tagType]] = {targetingConditions: JSON.stringify([{filters: targetingConditions[0].filters}])};
    return payload;
}

ListsApi.prototype.txtListAction = function (tagId, tagType, items, action) {
    items = items.filter(e => e !== '');
    if (items.length === 0) {
        return Promise.resolve();
    }
    winston.info(`(${tagId}) Trying to ${action.name} ${items.length} ${this.listType}s`);
    return this.api.getTag(tagId, tagType)
        .then(async res => {
            let targetingConditions = this.parseTargetingConditions(res.data[tagTypeMap[tagType]].targetingConditions);
            const filterObj = targetingConditions[0].filters.find((f) => this.possibleFilters.includes(f.key));
            const tagName = res.data[tagTypeMap[tagType]].name;

            if (filterObj && filterObj.rel === action.relTypeAdd) {
                winston.info(`(${tagId}) ${this.listType} ${action.relTypeAddName} filter found - adding to it`);
                return this.addToAutomatedList(filterObj, items, targetingConditions, tagId, tagName, tagType);
            }
            if (filterObj && filterObj.rel === action.relTypeRemove) {
                winston.info(`(${tagId}) ${this.listType} ${action.relTypeRemoveName} filter found - removing from it`);
                return this.removeFromAutomatedList(filterObj, items, targetingConditions, tagId, tagName, tagType);
            }

            if (action === actions.block) {
                winston.info(`(${tagId}) No ${this.listType} filter found. Creating a new one`);
                targetingConditions = await this.createTxtListFilter(targetingConditions, items, tagId, tagName, blackListRel);
                return this.api.updateTag(tagId, tagType, generateParamsFromTargetingConditions(tagType, targetingConditions))
            }
            winston.error(`(${tagId}) No ${this.listType} filter found - can't unblock`);
        })
};

ListsApi.prototype.unblockItemsFromTag = function (tagId, tagType, items) {
    return this.txtListAction(tagId, tagType, items, actions.unblock);
};

ListsApi.prototype.blockItemsFromTag = function (tagId, tagType, items) {
    return this.txtListAction(tagId, tagType, items, actions.block);
};

ListsApi.prototype.addItemsToWhitelist = async function (tagId, items, tagType) {
    return this.unblockItemsFromTag(tagId, tagType, items);
};

ListsApi.prototype.addToAutomatedList = async function (filterObj, items, targetingConditions, tagId, adSourceName, tagType) {
    const automatedList = await this.getAutomatedList(filterObj);

    if (automatedList === undefined) {
        return this.createListAddItemsAndUpdateTag(filterObj, items, targetingConditions, tagId, adSourceName, tagType)
    }
    return this.addItemsToTxtList(automatedList.id, items);
};

ListsApi.prototype.removeFromAutomatedList = async function (filterObj, items, targetingConditions, tagId, adSourceName, tagType) {
    const automatedList = await this.getAutomatedList(filterObj);

    if (automatedList === undefined) {
        winston.error(`${tagId} - ${adSourceName} : This demand should have an automated list - merging existing lists into new automated list`);
        const allListsItems = await this.getAllItemsFromLists(filterObj.values);
        const finalItems = allListsItems.filter(item => !items.includes(item));
        filterObj.values = [];
        return this.createListAddItemsAndUpdateTag(filterObj, finalItems, targetingConditions, tagId, adSourceName, tagType)
    }
    return this.removeItemsFromTxtList(automatedList.id, items);
};

ListsApi.prototype.createListAddItemsAndUpdateTag = async function (filterObj, items, targetingConditions, tagId, adSourceName, tagType) {
    const newListId = await this.createListAndAddItems(items, `${automatedListPrefix}${adSourceName}`);
    filterObj.values.push(newListId);
    return this.api.updateTag(tagId, tagType, generateParamsFromTargetingConditions(tagType, targetingConditions));
};

ListsApi.prototype.filterAndMap = function (rows) {
    return rows.filter(row => row.excluded === 1)
        .map(row => row[this.rowKey])
        .filter(item => !['', undefined, 'undefined'].includes(item));
};

ListsApi.prototype.getAllItemsFromLists = function (listIds) {
    return Promise.all(listIds.map(listId => this.getItemsInTxtList(listId)))
        .then(lists => {
            return _.union([].concat.apply([], lists));
        })
};

ListsApi.prototype.getTagsListsItems = function (tagBySupplies) {
    const domainsBySupply = {};
    const promises = Object.keys(tagBySupplies).map(supplyId => {
        const lists = JSON.parse(tagBySupplies[supplyId].targetingConditions)[0].filters
            .find(o => this.possibleFilters.includes(o.key))
            .values;
        return this.getAllItemsFromLists(lists)
            .then(domains => {
                domainsBySupply[supplyId] = domains;
            })
    });
    return Promise.all(promises).then(() => {
        return domainsBySupply;
    })
};

ListsApi.prototype.parseTargetingConditions = function (str) {
    return str ? JSON.parse(str) : [{filters: []}];
};

ListsApi.prototype.checkIfWhiteList = function (tagData) {
    return this.checkTargetingConditionsWhiteList(this.parseTargetingConditions(tagData.targetingConditions));
};

ListsApi.prototype.checkTargetingConditionsWhiteList = function (targetingConditions) {
    const filterObj = targetingConditions[0].filters.find((f) => this.possibleFilters.includes(f.key));

    return filterObj && filterObj.rel === whiteListRel;
};

ListsApi.prototype.checkTargetingConditionsBlackList = function (targetingConditions) {
    const filterObj = targetingConditions[0].filters.find((f) => this.possibleFilters.includes(f.key));

    return filterObj && filterObj.rel === blackListRel;
};

ListsApi.prototype.createTxtListFilter = async function (targetingConditions, items, tagId, adSourceName, relType) {
    const newListId = await this.createListAndAddItems(items, `${automatedListPrefix}${adSourceName}`);

    targetingConditions[0].filters.push(this.getFilterForList(newListId, relType));

    return targetingConditions;
};

ListsApi.prototype.setWhitelistFiltering = function (targetingConditions, listId) {
    const filters = this.parseTargetingConditions(targetingConditions);

    _.remove(filters[0].filters, (f) => this.possibleFilters.includes(f.key));
    filters[0].filters.push(this.getFilterForList(listId, whiteListRel));

    return JSON.stringify(filters);
};

ListsApi.prototype.getFilterForList = function (listId, relType) {
    return {
        key: this.jsonKey,
        rel: relType,
        values: [listId]
    }
};

ListsApi.prototype.createListAndAddItems = function (items, listName) {
    return this.createNewList(listName)
        .then((listData) => {
            return this.setTxtListItems(listData.id, items);
        })
};

ListsApi.prototype.setTxtListItems = function (listId, items) {
    const s = new Readable();
    s.push(items.join('\r\n'));
    s.push(null);
    s.path = 'data.json';

    return rp({
        method: 'POST',
        uri: `https://ssp-upload.streamrail.net/upload/${this.txtUploadUrl}/${listId}`,
        formData: {
            some: 'DATA',
            file: s
        },
        headers: {Authorization: `Bearer ${this.api.token}`}
    }).then(() => listId)
};

ListsApi.prototype.getItemsInTxtList = function (listId) {
    return this.getListData(listId)
        .then(res => {
            return axios.get(res.uploadedUrl)
        })
        .then(res => {
            return res.data
                .split('\n')
                .map(item => item.replace(/\r/g, ''))
        })
};

ListsApi.prototype.addItemsToTxtList = async function (listId, items) {
    const itemsInList = await this.getItemsInTxtList(listId);
    const newListItems = _.union(itemsInList, items);
    return this.setTxtListItems(listId, newListItems);
};

ListsApi.prototype.removeItemsFromTxtList = async function (listId, items) {
    const itemsInList = await this.getItemsInTxtList(listId);
    const newListItems = itemsInList.filter(listItem => !items.includes(listItem));

    if (newListItems.length === 0) {
        winston.error(`Removing all items from list ${listId}`);
    }
    return this.setTxtListItems(listId, newListItems);
};

ListsApi.prototype.getAutomatedList = function (filterObj) {
    return Promise.all(
        filterObj.values.map(listId => this.getListData(listId))
    ).then((arrayOfListsData) => {
        return arrayOfListsData.find(listData => listData.name.startsWith(automatedListPrefix));
    })
};

ListsApi.prototype.getListData = function (listId) {
    return axios.get(`${this.api.baseUrl}/${this.urlString}/${listId}`, this.api.headersObjWithToken())
        .then(res => res.data[this.jsonKey]);
};

ListsApi.prototype.createNewList = function (listName) {
    winston.info(`Creating a new list with name ${listName}`);
    const payload = {};
    payload[this.jsonKey] = {name: listName};

    return axios.post(`${this.api.baseUrl}/${this.urlString}`, payload, this.api.headersObjWithToken())
        .then(res => res.data[this.jsonKey])
        .catch(err => {
            if (err.response.data.errors.name[0] === this.listNameExistsError) {
                winston.info(`A list with this name already exists, appending (2) to list name`);
                return this.createNewList(`${listName}(2)`)
            }
            throw err;
        })
};
