"use strict";
const StreamrailApi = require('./api');
const automationMain = require('./../demand_geo_main');

const streamrailDemandGeoTable = 'streamrail_demand_geo';
const platform = 'streamrail';

function mapFieldsForLogic(rows) {
    return rows.map(row=>{
        return {
            demandPartner: row.advertiser,
            demandName: row.adSource,
            geo: row.country,
            requests: row.requests,
            impressions: row.impressions,
            id: row.adSourceId,
        }
    })
}

function mapFieldsForDb(rows) {
    return rows.map(row=>{
        return {
            ad_source: row.demandName,
            ad_source_id: row.id ? row.id : null,
            geo: row.geo,
            requests: row.requests,
            impressions: row.impressions,
            fillrate: row.fillRate,
            reqs_relative_to_tag: row.reqsRelToTag,
            imps_relative_to_tag: row.impsRelToTag,
            excluded: row.excluded || 0,
            info: row.info
        }
    })
}

function startAutomation(account) {
    const streamrailApi = new StreamrailApi(account.username, account.password);

    return automationMain(account, platform, streamrailApi, mapFieldsForDb, mapFieldsForLogic, streamrailDemandGeoTable);
}

module.exports = startAutomation;