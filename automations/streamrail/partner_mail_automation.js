const config = require('./../../config/config');
const StreamrailApi = require('./api');
const winston = require("winston");
const utils = require('../utils');
const util = require('util');
const _ = require('lodash');
const Promise = require('bluebird');
const mailFormats = require('./../mail/mail_formats');
const mailgun = require('./../mail/mailgun');

const automationType = 'partner_mail';

module.exports = start;

function partnerMailHandler(partnerName, wlData, partnerEmail) {

    const emailMessage = mailFormats.getStreamrailSupplyMailText(wlData);

    const recipients = ['automations@thetimmedia.com', 'alex.b@thetimmedia.com', 'aviv@thetimmedia.com'];
    partnerEmail ?
        recipients.push(partnerEmail.email) :
        winston.error(`No email found for partner: ${partnerName}`);
    // const recipients = ['daniel@thetimmedia.com']; // for dev

    winston.info(`Sending email to ${partnerName} : ${emailMessage}`);

    return mailgun.sendMailWithSignatureAndAttachment(recipients.join(','), `${partnerEmail.name} - Whitelist actions`, emailMessage, mailFormats.orenSignature)
        .then(()=>{
            winston.info(`Email sent to ${partnerName}`);
        })
}

function start(account) {
    const streamrailApi = new StreamrailApi(account.username, account.password);

    return streamrailApi.login()
        .then((token) => {
            winston.info(`Token acquired: ${token}`);
            return Promise.all([
                utils.getNewWhitelists(account.id),
                utils.getPartnerEmails(),
                utils.getFiltering(automationType, account.id),
            ])
        })
        .then(([wlData, partnerEmails, filters]) => {
            const wlDataByPartner = utils.applyFilters(_.groupBy(wlData, 'partner'), filters);

            const promises = Object.keys(wlDataByPartner).map(partnerName => {
                return partnerMailHandler(partnerName, wlDataByPartner[partnerName], partnerEmails[partnerName])
            });

            return Promise.all(promises);
        })
        .catch(err => {
            console.log(util.inspect(err));
        })
}