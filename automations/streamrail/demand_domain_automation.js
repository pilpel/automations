"use strict";
const StreamrailApi = require('./api');
const winston = require("winston");
const _ = require("lodash");
const getSettings = require('../get_settings');
const utils = require('../utils');
const util = require('util');
const Promise = require('bluebird');
const domainBlacklistLogic = require('../demand_domain_logic');
const mapObjectFields = require('./demand_domain_mapping');
const storeReport = require('../store_report');
const blockFromListTypeAndHandleOpposites = require('../opposite_logic');

const relevantListTypes = ['app_bundles', 'domains'];
const tableName = 'streamrail_demand_domain';
const automationType = 'demand_domain';
const tagTypeAdSources = 'ad-sources';
const platform = 'streamrail';
const concurrency = 1;

function startAutomation(account) {
    const streamrailApi = new StreamrailApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        utils.getFiltering(automationType, account.id),
        streamrailApi.login(),
        utils.getOppositeTagsMapping(account.id),
    ]).then(([, filters, token, oppositesMap]) => {
        winston.info(`Token acquired: ${token}`);

        return streamrailApi.getDemandsDomainReport()
            .then((report) => {
                return domainBlacklistLogic(mapObjectFields.mappingForLogic(report), filters);
            })
            .then((res) => {
                const report = res.rows;
                const demandList = Object.keys(report);

                const promiseProducer = function () {
                    if (demandList.length === 0) {
                        return null;
                    }
                    const demand = demandList.pop();
                    winston.info(`Starting promise chain for ${demand}`);
                    const demandRows = report[demand];
                    const demandId = demandRows[0].adSourceId;

                    let listTypeCounter = 0;
                    const listTypePromiseProducer = function () {
                        if (listTypeCounter >= relevantListTypes.length) {
                            return null;
                        }

                        const listType = relevantListTypes[listTypeCounter++];

                        return blockFromListTypeAndHandleOpposites(streamrailApi, listType, demandRows, demandId, oppositesMap, filters, account, demand, res.sumsByDemand, tagTypeAdSources)
                            .catch(err => {
                                const errorString = JSON.stringify(_.get(err, 'response.data.errors'));
                                winston.error(`Error data: ${errorString}`);
                                if (errorString.includes('Invalid environment for key')) {
                                    return
                                }
                                winston.error(`Error handling ${demandId}: ${util.inspect(err)}`);
                            });
                    };
                    return utils.fulfillSinglePromisesSync(listTypePromiseProducer)
                        .then(() => {
                            return storeRows(res.rows[demand]);
                        })
                        .then(utils.sleeper(1000))
                        .catch(err => {
                            winston.error(`Error handling ${demandId}: ${util.inspect(err)}`);
                        });
                };

                return utils.fulfillPromisesSync(promiseProducer, concurrency);
            })
    })
        .then(()=>{
            winston.info(`Erasing old DB data`);
            return utils.clearDbEntriesOlderThan(tableName, 14);
        })
        .then(res => {
            winston.info(`Streamrail demand domain automation finished`)
        })
        .catch(err => {
            winston.info(util.inspect(err));
        });
}

function storeRows(rows) {
    const mappedRows = mapObjectFields.mappingForDb(rows);
    return storeReport(mappedRows, tableName)
}

module.exports = startAutomation;