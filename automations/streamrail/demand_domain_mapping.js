const _ = require('lodash');
const utils = require('./../utils');

module.exports = {
    mappingForLogic,
    mappingForDb,
};

function mappingForLogic(rows) {
    rows.forEach(row=>{
        row.demand_tag_name = row.adSource;
        row.fill_rate = Number(row.requests) && Number(row.impressions) / Number(row.requests);
        row.demand_tag_id = row.adSourceId;
        row.partner = row.advertiser;
    });
    return rows;
}

function mappingForDb(rows) {

    if (rows.length > 20000) {
        const removed = _.remove(rows, (row)=>row.requests < 500);
        if (removed.length > 0) {
            rows.push({
                adSource: removed[0].adSource,
                adSourceId: removed[0].adSourceId,
                domain: `aggregated_domains_with_low_requests`,
                requests: _.sumBy(removed, row => Number(row.requests)),
                impressions: _.sumBy(removed, row => Number(row.impressions)),
                fillrate: 0,
                req_rel_to_tag: 0,
                imps_rel_to_tag: 0,
                timeout_rate: 0,
                excluded: 0,
                info: 'aggregated due to high number of domains with low requests'
            })
        }
    }

    return rows.map(row=>{
        return {
            demand_name: row.adSource,
            demand_id: row.adSourceId,
            domain: utils.removeBadChars(utils.prepForDb(row.domain || row.bundleId)),
            requests: row.requests,
            impressions: row.impressions,
            fillrate: row.fill_rate,
            req_rel_to_tag: row.req_rel_to_tag,
            imps_rel_to_tag: row.imps_rel_to_tag,
            excluded: row.excluded || 0,
            info: row.info
        }
    });
}
