const rp = require('request-promise');
const unzip = require('unzip2');
const dateFormat = require('dateformat');
const _ = require('lodash');
const axios = require("axios");
const https = require('https');
const winston = require('winston');
const csv = require('csvtojson');
const Promise = require('bluebird');
const utils = require('./../utils');
const ListsApi = require('./api_lists');
const pubMapping = require('./pub_mapping');

const baseUrl = `https://partners.streamrail.com/api/v2`;
const geoCustomReportId = `5a9e725cd25cdd0002143ddd`;
const domainCustomReportId = `5abcf9bc1df31800025be487`;
const supplyWlCustomReportId = `5ae85c30c289c10002e9fc89`;
const supplyDomainCustomReportId = `5afafed4bc45850002678131`;
const supplyLast7DaysCustomReportId = `5b165edeedc9c30002a081e9`;

const tagTypeTrafficChannels = 'traffic-channels';
const tagTypeAdSources = 'ad-sources';

class Api {
    constructor(user, pass) {
        this.user = user;
        this.pass = pass;
        this._listManagers = {
            app_bundles: new ListsApi(this, 'app'),
            domains: new ListsApi(this, 'domain'),
            country: new ListsApi(this, 'country'),
        };
        this.baseUrl = `https://partners.streamrail.com/api/v2`;
        this.platformShortName = {
            desktop: 'DT',
            mobile_app: 'IA',
            mobile_web: 'MW',
            ctv: 'CTV'
        };
    }

    get token() {
        return this._token;
    }

    get listManagers() {
        return this._listManagers;
    }

    set token(value) {
        this._token = value;
    }

    login() {
        const payload = `username=${this.user}&password=${this.pass}`;

        return axios.post(`${baseUrl}/login`, payload)
            .then(res => {
                this.token = res.data.access_token;
                return res.data.access_token;
            });
    }

    getSupplyReportLast7Days() {
        return _regenerateAndGetReport(this.token, supplyLast7DaysCustomReportId)
            .then(mapFields)
    }

    getDemandsDomainReport() {
        return _regenerateAndGetReport(this.token, domainCustomReportId);
    }

    getSupplyDomainReport() {
        return _regenerateAndGetReport(this.token, supplyDomainCustomReportId);
    }

    getDemandsGeoReport() {
        return _regenerateAndGetReport(this.token, geoCustomReportId);
    }

    async getSupplyWlReport() {
        const endDate = new Date();
        const startDate = new Date();
        startDate.setDate(startDate.getDate() - 1);

        await this.setCustomReportDateRange(supplyWlCustomReportId, startDate, endDate);
        await waitUntilReportIsDone(this.token, supplyWlCustomReportId);
        const rows = await getRows(this.token, supplyWlCustomReportId);

        return rows.filter(row => {
            return !(row.date === dateFormat(startDate, "yyyy-mm-dd") && Number(row.hour) < endDate.getUTCHours());
        });
    }

    setCustomReportDateRange(reportId, startDate, endDate) {
        return getCustomReport(this.token, reportId)
            .then(res => {
                const reportConfig = _.omit(res.data.customReport, ['table', 'totalRows', 'createdBy', 'createdOn', 'generating', 'id', 'isSystemReport', 'largeReport', 'lastExecuted', 'lastGenerated', 'lastModified', 'modifiedBy', 'org', 'reportStatus']);
                reportConfig.reportType = 0;
                reportConfig.reportActivityType = 2;
                reportConfig.status = 0;
                reportConfig.endDate = dateFormat(endDate, "yyyy-mm-dd");
                reportConfig.startDate = dateFormat(startDate, "yyyy-mm-dd");
                return axios.put(`${baseUrl}/custom-reports/${reportId}`, {customReport: reportConfig}, this.headersObjWithToken());
            })
    }

    async createOpposite(originalTagName, adSourceId, listType, items) {
        const params = {
            name: `opposite_tag_${originalTagName}_${new Date().toJSON().slice(0,10).replace(/-/g,'/')}`,
            requestCap: 100000,
            requestCapType: "evenly",
            status: 0,
        };
        const newListId = await this.listManagers[listType].createListAndAddItems(items, `Automated list ${params.name}`);
        params.targetingConditions = this.listManagers[listType].setWhitelistFiltering(null, newListId);
        return this.duplicateAdSource(adSourceId, params)
            .then((res=>{
                winston.info(`Tag ${originalTagName} duplicated. New tag id: ${res.data.adSource.id}`);
                return res.data.adSource;
            }))
    }

    duplicateAdSource(demandId, params) {
        return this.getAdSource(demandId)
            .then(res=>{
                const fieldsToRemove = ['createdOn', 'createdBy', 'dealType', 'exchangeConnections', 'globalImpressionCapLeft', 'id', 'isExchange', 'isMrktDummy', 'isPlatformTag', 'lastModified', 'modifiedBy', 'org', 'playerDimensions'];
                const payload = {
                    adSource: _.omit(Object.assign(res.data.adSource, params), fieldsToRemove)
                };

                return axios.post(`${baseUrl}/${tagTypeAdSources}`, payload, this.headersObjWithToken())
            })
    }

    async createSupplyWhitelistTag(tfId, partner, newRate, geos, domains, apps) {

        const originalTf = (await this.getTrafficChannel(tfId)).data.trafficChannel;
        const params = {
            cost: newRate,
            floor: Number(newRate) + 0.3,
        };

        partner = pubMapping[partner] || partner;
        const roundedRate = Math.round(newRate * 100) / 100;
        const shortEnv = this.platformShortName[originalTf.env];
        const playerSize = getPlayerSize(originalTf);
        const geoTargeting = geos.length > 2 ? 'WW' : geos.join('_');
        const domainTargeting = (domains && domains.length === 1)
            ? domains[0] : 'WL';
        const dateString = dateFormat(new Date(), 'ddmm');

        const newName = `[V]_${partner}_$${roundedRate}_${shortEnv}_${playerSize}_${geoTargeting}_${domainTargeting}_${dateString}`;

        params.name = newName;

        let newListId;
        if (domains && domains.length > 0) {
            newListId = await this.listManagers.domains.createListAndAddItems(domains, `Automated list ${newName}`);
            params.targetingConditions = this.listManagers.domains.setWhitelistFiltering(originalTf.targetingConditions, newListId);
        } else if (apps && apps.length > 0) {
            newListId = await this.listManagers.app_bundles.createListAndAddItems(apps, `Automated list ${newName}`);
            params.targetingConditions = this.listManagers.app_bundles.setWhitelistFiltering(originalTf.targetingConditions, newListId);
        } else {
            throw `No domain/app targeting has been specified`;
        }

        return this.duplicateTrafficChannel(tfId, params);
    }

    duplicateTrafficChannel(tfId, params) {
        return this.getTrafficChannel(tfId)
            .then(res=>{
                const fieldsToRemove = ['createdBy', 'createdOn', 'id', 'isExchange', 'lastModified', 'mobileTranscodeEnabled', 'modifiedBy', 'org', 'waterfalls', 'isMrktDummy'];
                const tfParams = _.omit(Object.assign(res.data.trafficChannel, params), fieldsToRemove);

                tfParams.dealTypeExceptions = tfParams.dealTypeExceptions || [];
                tfParams.eventIntegrations = tfParams.eventIntegrations || [];
                tfParams.exchangeConnections = tfParams.exchangeConnections || [];
                tfParams.status = 0;

                const payload = {
                    trafficChannel: tfParams
                };

                return axios.post(`${baseUrl}/${tagTypeTrafficChannels}`, payload, this.headersObjWithToken())
            })
            .catch(err =>{
                if (err.message !== 'Request failed with status code 400') {
                    throw err;
                }
                if (_.get(err, 'response.data.errors.name[0]') === `TrafficChannel with the name ${params.name} already exist`) {
                    params.name += '(2)';
                    return this.duplicateTrafficChannel(tfId, params);
                }
            })
    }

    blockGeosFromDemand(adSourceId, geos) {
        return this.listManagers.country.txtFieldBlock(adSourceId, geos);
    }

    unblockGeosFromDemand(adSourceId, geos) {
        return this.listManagers.country.txtFieldUnblock(adSourceId, geos);
    }

    geosFilterAndMap(rows) {
        return this.listManagers.country.filterAndMap(rows);
    }

    getTag(tagId, tagType) {
        return axios.get(`${baseUrl}/${tagType}/${tagId}`, this.headersObjWithToken())
    }

    updateTag(tagId, tagType, payload) {
        return axios.put(`${baseUrl}/${tagType}/${tagId}`, payload, this.headersObjWithToken())
    }

    getAdSource(adSourceId) {
        return this.getTag(adSourceId, tagTypeAdSources);
    }

    updateAdSource(adSourceId, payload) {
        return this.updateTag(adSourceId, tagTypeAdSources, payload);
    }

    getTrafficChannel(tfId) {
        return this.getTag(tfId, tagTypeTrafficChannels);
    }

    updateTrafficChannel(tfId, payload) {
        return this.updateTag(tfId, tagTypeTrafficChannels, payload);
    }

    getTrafficChannels(params = {}) {
        const payload = this.headersObjWithToken();
        payload.params = params;
        return axios.get(`${baseUrl}/traffic-channels`, payload)
    }

    headersObjWithToken() {
        return {headers: {Authorization: `Bearer ${this.token}`}};
    }

    supplyCountryTargetingIsSimilar(countries, tagData) {
        return countries.every((c) => tagData.name.includes(c));
    }
}

function mapFields(rows) {
    return rows.map(row=>{
        row.usable_requests = row.requests;
        row.supply_tag_id = row.trafficChannelId;
        return row;
    })
}

function _regenerateAndGetReport(token, reportId) {
    return regenerateCustomReport(token, reportId)
        .then((res)=>{
            return waitUntilReportIsDone(token, reportId)
        })
        .then((res)=>{
            winston.info(`Report generation complete, starting download`);
            return getRows(token, reportId)
        });
}

function regenerateCustomReport(token, reportId) {
    return axios.post(`${baseUrl}/generate-custom-report/${reportId}`, null, {headers: {Authorization: `Bearer ${token}`}})
}

function getRows(token, customReportId) {
    const options = {
        method: 'GET',
        followRedirect: false,
        simple: false,
        url: `https://partners.streamrail.com/data-export/data/custom/${customReportId}`,
        headers: {Authorization: `Bearer ${token}`}
    };

    return rp(options)
        .then(res => {
            return new Promise((resolve, reject)=>{
                https.get(res.split('="')[1].split('">')[0], function (response) {
                    response.pipe(unzip.Parse())
                        .on('entry', function (entry) {
                            csv()
                                .fromStream(entry)
                                .on('end_parsed', (rows) => {
                                    resolve(rows);
                                })
                                .on('done', error => {
                                    if (error) {
                                        reject(error)
                                    }
                                });
                        });
                })
            })
        })
        .catch(err=>{
            winston.error(err);
        })
}

function waitUntilReportIsDone(token, reportId) {
    return getCustomReport(token, reportId)
        .then(res=>{
            if (res.data.customReport.reportStatus === 'DONE') {
                return;
            }
            return Promise.delay(4000)
                .then(() => waitUntilReportIsDone(token, reportId));
        })
}

function getCustomReport(token, reportId) {
    return axios.get(`${baseUrl}/custom-reports/${reportId}`, {headers: {Authorization: `Bearer ${token}`}})
}

function getPlayerSize(tag) {
    return 'MIX'; // Currently, all tags get all kinds of player sizes
}

module.exports = Api;
