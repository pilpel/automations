module.exports = function mapping(rows) {
    return rows.map(row => {
        return {
            country_code: row.country,
            declared_domain: row.domain,
            supply_tag_name: row.trafficChannel,
            supply_tag_id: row.trafficChannelId,
            partner_name: row.publisher,
            partner_id: row.publisherId,
            impressions: Number(row.impressions),
            opportunities: Number(row.opportunities),
            requests: Number(row.requests),
            revenue: Number(row.demandRevenues),
            profit: Number(row.profit),
            app_bundle: row.bundleId,
            env: row.env
        }
    })
};