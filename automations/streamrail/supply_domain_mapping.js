const _ = require('lodash');
const utils = require('./../utils');

module.exports = {
    mappingForLogic,
    mappingForDb,
};

function mappingForLogic(rows) {
    rows.forEach(row=>{
        row.supply_tag_name = row.trafficChannel;
        row.supply_tag_id = row.trafficChannelId;
        row.fill_rate = Number(row.requests) && Number(row.impressions) / Number(row.requests);
        row.partner = row.publisher;
    });
    return rows;
}

function mappingForDb(rows) {

    if (rows.length > 20000) {
        const removed = _.remove(rows, (row)=>row.requests < 1000);
        if (removed.length > 0) {
            rows.push({
                supply_tag_name: removed[0].supply_tag_name,
                supply_tag_id: removed[0].supply_tag_id,
                domain: `aggregated_domains_with_low_requests`,
                publisher: removed[0].publisher,
                requests: _.sumBy(removed, row => Number(row.requests)),
                impressions: _.sumBy(removed, row => Number(row.impressions)),
                fill_rate: 0,
                req_rel_to_tag: 0,
                imps_rel_to_tag: 0,
                excluded: 0,
                info: 'aggregated due to high number of domains with low requests'
            })
        }
    }

    return rows.map(row=>{
        const obj = {
            supply_name: row.supply_tag_name,
            supply_id: row.supply_tag_id,
            partner: row.publisher,
            domain: (row.domain !== '') ? row.domain : row.bundleId,
            requests: row.requests,
            impressions: row.impressions,
            fillrate: row.fill_rate,
            req_rel_to_tag: row.req_rel_to_tag,
            imps_rel_to_tag: row.imps_rel_to_tag,
            excluded: row.excluded || 0,
            info: row.info
        };
        obj.domain = utils.removeBadChars(utils.prepForDb(obj.domain));
        return obj;
    })
}
