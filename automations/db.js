const mysql = require("mysql");
const config = require('../config/config');
const winston = require('winston');

function executeQuery(query, connectionName, params, logQuery) {

    return new Promise((resolve, reject) => {
        const con = mysql.createConnection(Object.assign({}, config.dbconnections[connectionName], {multipleStatements: true}));
        con.connect(function(err){
            if(err){
                return reject('Error connecting to Db report_results');
            }
            if (logQuery) {
                winston.info(con.format(query, params));
            }
            con.query(query, params, function(err, res){
                con.end(function(err) {
                });
                if(err) return reject(err);

                resolve(res);
            });
        });
    });
}

function executeQueryConfig(query, params, logQuery = true) {
    return executeQuery(query, 'terser_automation', params, logQuery)
}

module.exports = executeQueryConfig;