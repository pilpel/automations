const config = require('./../config/config');
const winston = require("winston");
const utils = require('./utils');
const util = require('util');
const _ = require('lodash');
const Promise = require('bluebird');
const mailFormats = require('./mail/mail_formats');
const mailgun = require('./mail/mailgun');
const executeQuery = require('./db');
const platforms = [
    {Api: require('./springserve/api'), name: 'springserve'},
    {Api: require('./streamrail/api'), name: 'streamrail'}
];

module.exports = start;

function setSupplyWlsToHasBeenActive(rowIds) {
    return executeQuery(`update supply_wl_tags set has_been_active = 1 WHERE tag_id in (${Array.from('?'.repeat(rowIds.length)).join(',')});`, rowIds)
}

function incrementFollowUps(rowIds) {
    return executeQuery(`update supply_wl_tags set follow_ups = follow_ups + 1 WHERE tag_id in (${Array.from('?'.repeat(rowIds.length)).join(',')});`, rowIds)
}

function getSupplyWlsToFollowUp(accountId) {
    const query = `SELECT * FROM supply_wl_tags WHERE account = ? and has_been_active = 0 and follow_ups < 2 and created_at <= curdate();`;
    return executeQuery(query, accountId)
}

function getNewlyActiveWls(suppliesToCheck, suppliesById) {
    return suppliesToCheck
        .filter((supplyId) => suppliesById[supplyId] && suppliesById[supplyId].usable_requests > 100);
}

function followUp(inactiveSupplies, allPartners) {

    const inactivesByPartner = {};

    inactiveSupplies.forEach(supplyTag => {
        if (allPartners[supplyTag.partner]) {
            const partnerName = allPartners[supplyTag.partner].name;
            inactivesByPartner[partnerName] = inactivesByPartner[partnerName] || [];
            inactivesByPartner[partnerName].push(supplyTag);
        }
    });

    const israeliPartners = Object.values(allPartners).filter(p => p.is_israeli === 1);

    const promises = Object.keys(inactivesByPartner).map(partnerName => {
        if (israeliPartners.includes(partnerName) && utils.isWeekendInIsrael()) {
            return;
        }
        if (!israeliPartners.includes(partnerName) && utils.isWeekend()) {
            return;
        }

        const recipients = ['automations@thetimmedia.com', 'alex.b@thetimmedia.com', 'aviv@thetimmedia.com'];

        // allPartners[partnerName] ?
        //     recipients.push(allPartners[partnerName].email) :
        //     winston.error(`No email found for partner: ${partnerName}`);

        winston.info(`Sending follow up to ${partnerName} : ${mailFormats.getFollowUpEmail(inactivesByPartner[partnerName])}`);

        return mailgun.sendMailWithSignatureAndAttachment(recipients,
            `Follow up email for ${partnerName}`,
            mailFormats.getFollowUpEmail(inactivesByPartner[partnerName]),
            mailFormats.orenSignature)
            .then(()=> inactivesByPartner[partnerName].map(i => i.tag_id) )
    });
    return Promise.all(promises);
}



async function getPlatformInactives(platformObj) {
    const account = Object.values(config.accounts).find(account => account.platform === platformObj.name);

    if (account === undefined) {
        return Promise.resolve();
    }

    const api = new platformObj.Api(account.username, account.password);
    await api.login();

    return Promise.all([
        api.getSupplyReportLast7Days(),
        getSupplyWlsToFollowUp(account.id),
    ])
        .then(async ([allSupplies, wlData])=>{
            const suppliesById = _.keyBy(allSupplies, 'supply_tag_id');
            const wlDataById = _.keyBy(wlData, 'tag_id');

            const newlyActiveWls = getNewlyActiveWls(Object.keys(wlDataById), suppliesById);
            if (newlyActiveWls.length > 0) {
                await setSupplyWlsToHasBeenActive(newlyActiveWls);
            }

            const inactiveSupplies = _.omit(wlDataById, newlyActiveWls);

            return inactiveSupplies;
        })
}

function start() {

    const promises = platforms.map(platformObj => getPlatformInactives(platformObj));
    promises.push(utils.getPartnerEmails());
    const inactiveSupplies = [];

    return Promise.all(promises)
        .then((res)=>{
            const allPartners = res.pop();
            res.filter(platformReturnValue => platformReturnValue !== undefined).forEach(platformInactives => {
                inactiveSupplies.push(...Object.values(platformInactives))
            });

            return followUp(inactiveSupplies, allPartners);
        })
        .then(arraysOfSupplyIds=>{
            const supplyIdsToInc = [].concat(arraysOfSupplyIds);
            if (supplyIdsToInc.length > 0) {
                return incrementFollowUps(supplyIdsToInc);
            }
        });
}