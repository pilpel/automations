const executeQueryConfig = require('./db');
const config = require('../config/config');

module.exports = getSettings;

function getSettings(platformName, automationType) {
    const query = `select * from automation_settings where subject=? and automation_type=?;`;
    config.automationSettings = {};

    return executeQueryConfig(query, [platformName, automationType])
        .then((rows) => {
            rows.forEach(row=>{
                config.automationSettings = config.automationSettings || {};

                config.automationSettings[row.name] = row.value;
            })
        });
}