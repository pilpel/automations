This document covers both Springserve and Streamrail APIs

API locations:
- Springserve: automations/springserve/api.js
- Streamrail: automations/streamrail/api.js

Links to official docs:
- [SpringServe](https://springserve.atlassian.net/wiki/spaces/SSD/pages/12517384/SpringServe+API+SDK)
- [Streamrail](https://partners-api-docs.streamrail.com/)

Links to platforms:
- [SpringServe](https://video.springserve.com/)
- [Streamrail](https://partners.streamrail.com/#/report)

The API is a JS class that must be instantiated:

`const SpringserveApi = require('./api');`<br>
`const springserveApi = new SpringserveApi(account.username, account.password);`<br>
`springserveApi.login();`

All methods are asynchronous.

api_lists.js is another JS class that is instantiated whenever the api is called.
It handles lists of type domains and app bundles, and in streamrail also countries.

Lists are managed as follows:
- All lists managed by the automations are named 'Automated list <tag name>'.
- If an automated list doesn't exist for a tag, it is created and attached to the tag.
- When trying to block a domain from a whitelist and an automated list doesn't exist, all existing lists will be merged into an automated one.

Troubleshooting API methods
- Try to replicate the problem on an inactive tag
- Set a breakpoint on the relevant method
- Use test_api.js (base dir) to isolate the problematic API method