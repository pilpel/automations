const config = require('./../../config/config');
const SpringserveApi = require('./api');
const winston = require("winston");
const utils = require('../utils');
const util = require('util');
const _ = require('lodash');
const getSettings = require('../get_settings');
const Promise = require('bluebird');
const supplyWlLogic = require('./../supply_wl_logic');
const filterExistingWhitelists = require('./../whitelist_similarity').filterExistingWhitelists;
const mapObjectFields = require('./supply_wl_mapping');

const tagTypeSupply = 'supply';
const tagTypeDemand = 'demand';
const automationType = 'supply_wl';
const platform = 'springserve';
const concurrency = 1;

module.exports = start;

function start(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        springserveApi.login(),
    ]).then(([,token]) => {
        winston.info(`Token acquired: ${token}`);
        return Promise.all([
            springserveApi.getSupplyWlReport(),
            utils.getFiltering(automationType, account.id),
            springserveApi.getAllActiveTags(tagTypeDemand),
            springserveApi.getAllActiveTags(tagTypeSupply),
            utils.getAlreadyOpenedWlPartners(account.id)
        ])
    })
        .then(async ([rows, filters, allDemands, allSupplies, partnersNotToOpen])=>{
            const mappedRows = mapObjectFields(rows);
            const suppliesObject = {};
            allSupplies.data.forEach(s=>suppliesObject[s.id] = s);

            const whitelists = supplyWlLogic(mappedRows, suppliesObject, filters, partnersNotToOpen);
            const newWhitelists = await filterExistingWhitelists(springserveApi, whitelists, suppliesObject, _.groupBy(mappedRows, 'supply_tag_id'));

            const promiseProducer = function () {
                if (_.isEmpty(newWhitelists)) {
                    return null;
                }

                const originalTagName = Object.keys(newWhitelists)[0];
                winston.info(`Starting WL promise chain for ${originalTagName}`);
                const wlData = newWhitelists[originalTagName];
                delete newWhitelists[originalTagName];

                winston.info(`Creating duplicate of supplyId ${wlData.supplyId}. price: ${wlData.price}. countries: ${wlData.countries}. domains: ${wlData.domains}. app_bundles: ${wlData.app_bundles}`);
                return springserveApi.createSupplyWhitelistTag(wlData.supplyId, wlData.partner, wlData.price, allDemands.data, wlData.countries, wlData.domains, wlData.app_bundles)
                    .then((newTag)=>{
                        return utils.storeWl(account.id, platform, wlData.partner, wlData.supplyId, newTag.data.id, newTag.data.name, newTag.data.rate, wlData.countries, wlData.domains, wlData.app_bundles, wlData.margin, newTag.data.environment);
                    })
                    .catch(err=>{
                        winston.error(`Error handling supplyId ${wlData.supplyId}: ${util.inspect(err)}`);
                    })
            };

            return utils.fulfillPromisesSync(promiseProducer, concurrency)
        })
        .catch(err=>{
            console.log(util.inspect(err));
            throw err;
        })
}
