const _ = require('lodash');
const utils = require('./../utils');

module.exports = {
    mappingForLogic,
    mappingForDb,
};

function mappingForLogic(rows) {
    rows.forEach(row=>{
        row.requests = row.usable_requests;
        row.impressions = row.total_impressions;
        row.partner = row.supply_partner_name;
        row.env = row.environment;
    });
    return rows;
}

function mappingForDb(rows) {

    if (rows.length > 20000) {
        const removed = _.remove(rows, (row)=>row.usable_requests < 1000);
        if (removed.length > 0) {
            rows.push({
                supply_tag_name: removed[0].supply_tag_name,
                supply_tag_id: removed[0].supply_tag_id,
                declared_domain: `aggregated_domains_with_low_requests`,
                supply_partner_name: removed[0].supply_partner_name,
                usable_requests: _.sumBy(removed, row => Number(row.usable_requests)),
                total_requests: _.sumBy(removed, row => Number(row.total_requests)),
                total_impressions: _.sumBy(removed, row => Number(row.total_impressions)),
                fill_rate: 0,
                req_rel_to_tag: 0,
                imps_rel_to_tag: 0,
                timeout_rate: 0,
                excluded: 0,
                info: 'aggregated due to high number of domains with low requests'
            })
        }
    }

    return rows.map(row=>{
        return {
            supply_name: row.supply_tag_name,
            supply_id: row.supply_tag_id,
            partner: row.supply_partner_name,
            domain: (row.declared_domain !== 'unknown') ? utils.prepForDb(row.declared_domain) : utils.prepForDb(row.app_bundle),
            requests: row.usable_requests,
            total_requests: row.total_requests,
            impressions: row.total_impressions,
            fillrate: row.fill_rate,
            req_rel_to_tag: row.req_rel_to_tag,
            imps_rel_to_tag: row.imps_rel_to_tag,
            timeout_rate: row.timeout_rate,
            excluded: row.excluded || 0,
            info: row.info
        }
    })
}
