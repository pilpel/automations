const _ = require('lodash');
const utils = require('./../utils');

module.exports = {
    mappingForDb,
    mappingForLogic,
};

function mappingForDb(rows) {
    return rows.map(row=>{
        return {
            demand_name: row.demand_tag_name,
            demand_id: row.demand_tag_id,
            domain: (row.declared_domain !== 'unknown') ? utils.prepForDb(row.declared_domain) : utils.prepForDb(row.app_bundle),
            requests: row.demand_requests,
            impressions: row.impressions,
            fillrate: row.fill_rate,
            req_rel_to_tag: row.req_rel_to_tag,
            imps_rel_to_tag: row.imps_rel_to_tag,
            timeout_rate: row.timeout_rate,
            excluded: row.excluded || 0,
            info: row.info
        }
    });
}

function mappingForLogic(rows) {
    rows.forEach(row=>{
        row.requests = row.demand_requests;
        row.partner = row.demand_partner_name;
        row.env = row.environment;
    });
    return rows;
}