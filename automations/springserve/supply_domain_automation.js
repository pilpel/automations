"use strict";
const SpringserveApi = require('./api');
const winston = require("winston");
const _ = require("lodash");
const getSettings = require('../get_settings');
const utils = require('../utils');
const util = require('util');
const Promise = require('bluebird');
const domainBlacklistLogic = require('../supply_domain_logic');
const mapObjectFields = require('./supply_domain_mapping');
const storeReport = require('../store_report');

const tagType = 'supply';
const automationType = 'supply_domain';
const platform = 'springserve';
const tableName = 'springserve_supply_domain';
const concurrency = 1;

function startAutomation(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        utils.getFiltering(automationType, account.id),
        springserveApi.login()
    ]).then(([, filters, token]) => {
        winston.info(`Token acquired: ${token}`);
        return springserveApi.getSupplyDomainReport()
            .then((report) => {
                return domainBlacklistLogic(mapObjectFields.mappingForLogic(report), filters);
            })
            .then((res) => {
                const report = res.rows;
                const supplyList = Object.keys(report);

                const promiseProducer = function () {
                    if (supplyList.length === 0) {
                        return null;
                    }
                    const supply = supplyList.pop();
                    const supplyRows = report[supply];
                    const supplyId = supplyRows[0].supply_tag_id;
                    winston.info(`Starting promise chain for ${supply} (${supplyId})`);

                    return Promise.all(
                        Object.keys(springserveApi.listManagers)
                            .map(listType => {
                                const itemsToBlock = springserveApi.listManagers[listType].filterAndMap(supplyRows);
                                return springserveApi.listManagers[listType].blockItemsFromTag(supplyId, tagType, itemsToBlock)
                                    .then(()=> {winston.info(`Supply (${supplyId}) Finished blocking ${itemsToBlock.length} ${listType}`)})
                            })
                        )
                        .then(() => {
                            return storeRows(res.rows[supply]);
                        })
                        .then(utils.sleeper(2000))
                        .catch(err => {
                            winston.error(`Error handling ${supplyId}: ${util.inspect(err)}`);
                        })
                };

                return utils.fulfillPromisesSync(promiseProducer, concurrency);
            })
    })
        .then(()=>{
            winston.info(`Erasing old DB data`);
            return utils.clearDbEntriesOlderThan(tableName, 14);
        })
        .then(res => {
            winston.info(`Springserve supply domain automation finished`)
        })
        .catch(err => {
            winston.info(util.inspect(err));
            throw err;
        });
}

function storeRows(rows) {
    return storeReport(mapObjectFields.mappingForDb([].concat.apply([], rows)), tableName)
}

module.exports = startAutomation;