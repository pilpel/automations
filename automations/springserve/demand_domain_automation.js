"use strict";
const SpringserveApi = require('./api');
const winston = require("winston");
const _ = require("lodash");
const getSettings = require('../get_settings');
const utils = require('../utils');
const util = require('util');
const Promise = require('bluebird');
const domainBlacklistLogic = require('../demand_domain_logic');
const mapObjectFields = require('./demand_domain_mapping');
const storeReport = require('../store_report');
const config = require('./../../config/config');
const blockFromListTypeAndHandleOpposites = require('../opposite_logic');

const tagType = 'demand';
const tableName = 'springserve_demand_domain';
const automationType = 'demand_domain';
const platform = 'springserve';
const concurrency = 1;

function startAutomation(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        utils.getFiltering(automationType, account.id),
        springserveApi.login(),
        utils.getOppositeTagsMapping(account.id),
    ]).then(([, filters, token, oppositesMap]) => {
        winston.info(`Token acquired: ${token}`);

        return springserveApi.getDemandsDomainReport()
            .then((report) => {
                return domainBlacklistLogic(mapObjectFields.mappingForLogic(report), filters);
            })
            .then((res) => {
                const report = res.rows;
                const demandList = Object.keys(report);

                const demandPromiseProducer = function () {
                    if (demandList.length === 0) {
                        return null;
                    }
                    const demand = demandList.pop();
                    const demandRows = report[demand];
                    const demandId = demandRows[0].demand_tag_id;
                    winston.info(`Starting promise chain for (${demandId}) ${demand}`);

                    let listTypeCounter = 0;
                    const listTypePromiseProducer = function () {
                        if (listTypeCounter >= Object.keys(springserveApi.listManagers).length) {
                            return null;
                        }

                        const listType = Object.keys(springserveApi.listManagers)[listTypeCounter];
                        listTypeCounter++;

                        return blockFromListTypeAndHandleOpposites(springserveApi, listType, demandRows, demandId, oppositesMap, filters, account, demand, res.sumsByDemand, tagType)
                            .catch(err => {
                                winston.error(`Error handling ${demandId}: ${util.inspect(err)}`);
                            });
                    };

                    return utils.fulfillSinglePromisesSync(listTypePromiseProducer)
                        .then(() => {
                            return storeRows(res.rows[demand]);
                        })
                        .then(utils.sleeper(1000))
                        .catch(err => {
                            winston.error(`Error handling ${demandId}: ${util.inspect(err)}`);
                        });
                };

                return utils.fulfillPromisesSync(demandPromiseProducer, concurrency);
            })
    })
        .then(()=>{
            winston.info(`Closing floating duplicates`);
            return springserveApi.closeFloatingDuplicates(tagType);
        })
        .then(()=>{
            winston.info(`Erasing old DB data`);
            return utils.clearDbEntriesOlderThan(tableName, 14);
        })
        .then(res => {
            winston.info(`Springserve demand domain automation finished`)
        })
        .catch(err => {
            winston.info(util.inspect(err));
            throw err;
        });
}

function storeRows(rows) {
    return storeReport(mapObjectFields.mappingForDb([].concat.apply([], rows)), tableName)
}

module.exports = startAutomation;