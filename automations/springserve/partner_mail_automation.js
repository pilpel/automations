const config = require('./../../config/config');
const SpringserveApi = require('./api');
const winston = require("winston");
const utils = require('../utils');
const util = require('util');
const _ = require('lodash');
const Promise = require('bluebird');
const mailFormats = require('./../mail/mail_formats');
const mailgun = require('./../mail/mailgun');

const tagTypeSupply = 'supply';
const blackListTableName = 'springserve_supply_domain';
const blacklistTagsInEmail = 5;
const blacklistItemsInEmail = 100;
const automationType = 'partner_mail';

module.exports = start;

function getTagsWithHighestReqs(blDataBySupply, tagsAmount) {
    const reqsDiff = [];
    _.forOwn(blDataBySupply, (supplyRows, supplyId)=>{
        reqsDiff.push({supplyId, diff: _.sumBy(supplyRows, (row=>row.total_requests)) - _.sumBy(supplyRows, (row=>row.requests))});
    });
    return _.sortBy(Object.values(reqsDiff), ['diff']).slice(-tagsAmount).map(diffObj => diffObj.supplyId);
}

function getDomainsWithHighestReqs(rows, domainsAmount) {
    return _.sortBy(rows, [function (row) {return row.total_requests - row.requests}]).slice(-domainsAmount).map(row=> row.domain);
}

function getEmailAttachments(domainsToEmailPerTag, suppliesObject) {
    return domainsToEmailPerTag.map((supplyObj) => {
        const supplyFromPlatform = suppliesObject[supplyObj.supplyId];
        if (!supplyFromPlatform) {
            return undefined
        }
        let filename = `TIM_${supplyFromPlatform.environment}`;
        filename += `_${supplyFromPlatform.rate}_SpringServe`;
        filename += `${(supplyFromPlatform.direct_connect) ? '_DC' : ''}`;
        filename += `_${supplyObj.supplyId}_blacklist.txt`;

        return mailgun.generateAttachment(filename, new Buffer(supplyObj.domains.join('\r\n'), 'utf-8'))
    });
}

function partnerMailHandler(partnerName, blData, wlData, suppliesObject, partnerEmail) {
    const blDataBySupply = _.groupBy(blData, 'supply_id');
    const tagsWithHighestReqs = getTagsWithHighestReqs(blDataBySupply, blacklistTagsInEmail);

    const domainsToEmailPerTag = tagsWithHighestReqs.map(supplyId => {
        return {
            supplyId,
            domains: getDomainsWithHighestReqs(blDataBySupply[supplyId], blacklistItemsInEmail)
        }
    });

    const emailMessage = mailFormats.getSpringSupplyMailText(wlData, domainsToEmailPerTag);
    const attachments = getEmailAttachments(domainsToEmailPerTag, suppliesObject);
    _.remove(attachments, (a) => a === undefined);

    if (attachments.length === 0 && wlData === undefined) {
        return;
    }

    const recipients = ['automations@thetimmedia.com', 'alex.b@thetimmedia.com', 'aviv@thetimmedia.com'];
    partnerEmail ?
        recipients.push(partnerEmail.email) :
        winston.error(`No email found for partner: ${partnerName}`);

    winston.info(`Sending email to ${partnerName} : ${emailMessage}`);

    return mailgun.sendMailWithSignatureAndAttachment(recipients.join(','), `${partnerName} - Blacklist/Whitelist actions`, emailMessage, mailFormats.orenSignature, attachments)
        .then(()=>{
            winston.info(`Email sent to ${partnerName} with ${attachments.length} blacklists`);
        })
}

function start(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);

    return springserveApi.login()
        .then((token) => {
            winston.info(`Token acquired: ${token}`);
            return Promise.all([
                springserveApi.getAllActiveTags(tagTypeSupply),
                utils.getTodaysSupplyBlacklistData(blackListTableName),
                utils.getNewWhitelists(account.id),
                utils.getPartnerEmails(),
                utils.getFiltering(automationType, account.id),
            ])
        })
        .then(([allSupplies, blData, wlData, partnerEmails, filters]) => {
            _.remove(blData, (row)=> ['unclassified', 'unknown'].includes(row.domain));
            const suppliesObject = {};
            allSupplies.data.forEach(s=>suppliesObject[s.id] = s);
            const blDataByPartner = utils.applyFilters(_.groupBy(blData, 'partner'), filters);
            const wlDataByPartner = utils.applyFilters(_.groupBy(wlData, 'partner'), filters);

            const allPartners = _.union(Object.keys(blDataByPartner), Object.keys(wlDataByPartner));

            const promises = allPartners.map(partnerName => {
                return partnerMailHandler(partnerName, blDataByPartner[partnerName], wlDataByPartner[partnerName], suppliesObject, partnerEmails[partnerName])
            });

            return Promise.all(promises);
        })
        .catch(err => {
            console.log(util.inspect(err));
        })
}