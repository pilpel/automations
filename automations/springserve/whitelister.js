const config = require('./../../config/config');
const SpringserveApi = require('./api');
const winston = require("winston");
const utils = require('../utils');
const util = require('util');
const _ = require('lodash');
const PromisePool = require('es6-promise-pool');
const getSettings = require('../get_settings');
const Promise = require('bluebird');
const automationType = 'whitelister';
const platform = 'springserve';

module.exports = start;

function start(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);
    return Promise.all([
        getSettings(platform, automationType),
        springserveApi.login()
    ]).then(([,token]) => {
            winston.info(`Token acquired: ${token}`);
            return springserveApi.getSupplyDomainReport()
        })
        .then(rows=>{
            const suppliesToConvert = [];

            const totalRequests = _.sumBy(rows, row => Number(row.usable_requests));
            const rowsBySupply = _.groupBy(rows, 'supply_tag_id');

            _.forOwn(rowsBySupply, (rowsOfSupply, supplyId)=>{

                const tagReqs = _.sumBy(rowsOfSupply, row => Number(row.usable_requests));
                const unclassified = rowsOfSupply.find(row=>row.declared_domain==='unclassified');

                if (unclassified && unclassified.usable_requests / tagReqs > config.automationSettings.unclassified_reqs_percent && tagReqs / totalRequests > config.automationSettings.rel_tag_reqs) {

                    winston.info(`Supply ${unclassified.supply_tag_name} (${supplyId}) has ${unclassified.usable_requests} unclassified requests and will be converted to whitelist`);

                    const domainsToKeep = rowsOfSupply
                        .filter(row=>Number(row.usable_requests) > 0 && row.declared_domain !== 'unclassified')
                        .map(row=>row.declared_domain);
                    suppliesToConvert.push({supplyId, domains: domainsToKeep})
                }
            });

            const promiseProducer = function promiseProducer() {
                if (suppliesToConvert.length === 0) {
                    return null;
                }
                const supply = suppliesToConvert.pop();
                winston.info(`Starting promise chain for ${supply.supplyId}`);

                return springserveApi.convertToDomainWhitelist(supply.supplyId, 'supply', supply.domains)
                    .then(utils.sleeper(2000));
            };

            return utils.fulfillPromisesSync(promiseProducer, 1);
        })
        .catch(err=>{
            console.log(util.inspect(err));
        })
}