"use strict";
const SpringserveApi = require('./api');
const automationMain = require('./../demand_geo_main');

const springServeDemandGeoTable = 'springserve_demand_geo';
const platform = 'springserve';

function mapFieldsForLogic(rows) {
    return rows.map(row=>{
        return {
            demandPartner: row.demand_partner_name,
            demandName: row.demand_tag_name,
            geo: row.country_code,
            requests: row.demand_requests,
            impressions: row.impressions,
            id: row.demand_tag_id,
        }
    })
}

function mapFieldsForDb(rows) {
    return rows.map(row => {
        return {
            demand_name: row.demandName,
            demand_id: row.id,
            geo: row.geo,
            requests: row.requests,
            impressions: row.impressions,
            fillrate: row.fillRate,
            reqs_relative_to_tag: row.reqsRelToTag,
            imps_relative_to_tag: row.impsRelToTag,
            excluded: row.excluded || 0,
            info: row.info
        }
    });
}

function startAutomation(account) {
    const springserveApi = new SpringserveApi(account.username, account.password);

    return automationMain(account, platform, springserveApi, mapFieldsForDb, mapFieldsForLogic, springServeDemandGeoTable);
}

module.exports = startAutomation;