module.exports = function mapping(rows) {
    return rows.map(row=>{
        row.partner_name = row.supply_partner_name;
        row.partner_id = row.supply_partner_id;
        row.requests = row.usable_requests;
        row.impressions = row.total_impressions;
        row.env = row.environment;
        return row;
    })
};