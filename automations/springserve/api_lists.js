const _ = require('lodash');
const axios = require("axios");
const winston = require('winston');
const Promise = require('bluebird');
const util = require('util');

module.exports = ListsApi;

function ListsApi(api, listType){
    this.listType = listType;
    this.urlPath = listTypeMapping[listType].urlPath;
    this.targetingKey = listTypeMapping[listType].targetingKey;
    this.listIdsKey = listTypeMapping[listType].listIdsKey;
    this.rowKey = listTypeMapping[listType].rowKey;
    this.itemInListKey = listTypeMapping[listType].itemInListKey;
    this.api = api;
}
const EXCLUDE = 'Exclude';
const INCLUDE = 'Include';
const BLACK_LIST = 'Blacklist';
const WHITE_LIST = 'Whitelist';
const TARGETING_ALL = 'All';
const deleteBatchSize = 100;
const postBatchSize = 500;
const baseUrl = `https://video.springserve.com`;
const LIST_TYPE_DOMAINS = 'domains';
const LIST_TYPE_APP_BUNDLES = 'app_bundles';
const listTypeMapping = {
    domains: {
        urlPath: 'domain_lists',
        targetingKey: 'domain_targeting',
        listIdsKey: 'domain_list_ids',
        rowKey: 'declared_domain',
        itemInListKey: 'name',
    },
    app_bundles: {
        urlPath: 'app_bundle_lists',
        targetingKey: 'app_bundle_targeting',
        listIdsKey: 'app_bundle_list_ids',
        rowKey: 'app_bundle',
        itemInListKey: 'app_bundle',
    }
};

ListsApi.prototype.getPayloadAndRemoveBadNames = function (entities) {
    let payload;
    const removed = _.remove(entities, (item) => item.includes('"'));
    if (removed.length > 0) {
        winston.error(`Bad names found: ${removed}`);
    }
    if (this.listType === LIST_TYPE_DOMAINS) {
        payload = {names : entities};
    }
    if (this.listType === LIST_TYPE_APP_BUNDLES) {
        payload = {app_bundles : entities};
    }
    return payload;
};

ListsApi.prototype.getList = function (listId) {
    return axios.get(`${baseUrl}/api/v0/${this.urlPath}/${listId}`, {headers: {Authorization: `${this.api.token}`}})
        .then(res=>{
            return res.data;
        })
};

ListsApi.prototype.getAutomatedList = function (listIds) {
    return Promise.all(listIds.map(listId=>this.getList(listId)))
        .then(lists=>{
            return lists.find(element=>element.name.startsWith('Automated'));
        })
};

ListsApi.prototype.addToList = function (listId, entities) {
    const payload = this.getPayloadAndRemoveBadNames(entities);
    return axios.post(`${baseUrl}/api/v0/${this.urlPath}/${listId}/${this.listType}/bulk_create`, payload, {headers: {Authorization: `${this.api.token}`}})
};

ListsApi.prototype.addToListInBatches = function (listId, entities, batchSize = postBatchSize) {
    return this.addToList(listId, entities.slice(0, batchSize))
        .then(res=>{
            if (entities.length < batchSize) {
                return res;
            }
            return this.addToListInBatches(listId, entities.slice(batchSize), batchSize)
        });
};

ListsApi.prototype.removeFromList = function (listId, entities) {
    const payload = this.getPayloadAndRemoveBadNames(entities);
    return axios.delete(`${baseUrl}/api/v0/${this.urlPath}/${listId}/${this.listType}/bulk_delete`, {params: payload, headers: {Authorization: `${this.api.token}`}})
};

ListsApi.prototype.removeFromListInBatches = function (listId, entities, batchSize = deleteBatchSize) {
    return this.removeFromList(listId, entities.slice(0, batchSize))
        .then(res=>{
            if (entities.length < batchSize) {
                return res;
            }
            return this.removeFromListInBatches(listId, entities.slice(batchSize), batchSize)
        });
};

ListsApi.prototype.detachListsFromTag = function (tagId, tagType) {
    return this.api.getTag(tagId, tagType)
        .then(tag=>{
            winston.info(`detaching list from ${tagType} ${tagId}`);
            winston.info(`previously attached: ${tag.data.domain_list_ids}`);

            const payload = {};
            payload[this.targetingKey] = TARGETING_ALL;
            payload[this.listIdsKey] = [];

            return this.api.editTag(tagId, tagType, payload);
        })
};

ListsApi.prototype.createAutomatedList = function (tagName) {
    return axios.post(`${baseUrl}/api/v0/${this.urlPath}`, {name: `Automated list ${tagName}`}, {headers: {Authorization: `${this.api.token}`}});
};

ListsApi.prototype.createAndAttachList = function (tag, tagType, entities, excludeType) {
    return this.createAutomatedList(tag.name)
        .then(automatedList => {
            winston.info(`${tagType} ${tag.name} New automated list: ${automatedList.data.id}`);
            tag[this.listIdsKey].push(automatedList.data.id);

            const payload = {};
            payload[this.targetingKey] = excludeType;
            payload[this.listIdsKey] = tag[this.listIdsKey];

            return this.addToListInBatches(automatedList.data.id, entities)
                .then(()=>{
                    return this.api.editTag(tag.id, tagType, payload)
                });
        })
};

ListsApi.prototype.createAndReplaceList = function (tag, tagType, entities, excludeType) {
    return this.createAutomatedList(tag.name)
        .then(automatedList => {
            winston.info(`${tagType} ${tag.name} New automated list: ${automatedList.data.id}`);

            const payload = {};
            payload[this.targetingKey] = excludeType;
            payload[this.listIdsKey] = [automatedList.data.id];

            return this.addToListInBatches(automatedList.data.id, entities)
                .then(()=>{
                    return this.api.editTag(tag.id, tagType, payload)
                });
        })
};

ListsApi.prototype.getItemsInList = function (listId) {
    return axios.get(`${baseUrl}/api/v0/${this.urlPath}/${listId}/${this.listType}`, {headers: {Authorization: `${this.api.token}`}})
        .then(res=>{
            return res.data.map(d=>d[this.itemInListKey]);
        })
};

ListsApi.prototype.getAllItemsFromLists = function (listIds) {
    return Promise.all(listIds.map(listId=>this.getItemsInList(listId)))
        .then(lists=>{
            return _.union([].concat.apply([], lists));
        })
};

ListsApi.prototype.getTagsListsItems = function (tagBySupplies) {
    const domainsBySupply = {};
    const promises = Object.keys(tagBySupplies).map(supplyId=>{
        return this.getAllItemsFromLists(tagBySupplies[supplyId][this.listIdsKey])
            .then(domains=>{
                domainsBySupply[supplyId] = domains;
            })
    });
    return Promise.all(promises).then(()=>{
        return domainsBySupply;
    })
};

ListsApi.prototype.removeItemsFromWhitelist = function (tag, entities, tagType, tagId) {
    winston.info(`${tagType} ${tag.data.name} has a whitelist, trying to remove ${entities.length} items`);
    return this.getAutomatedList(tag.data[this.listIdsKey])
        .then(automatedList => {
            if (automatedList && tag.data[this.listIdsKey].length === 1) {
                return this.getItemsInList(automatedList.id)
                    .then(wlItems => {
                        if (wlItems.length > entities.length) {
                            return this.removeFromListInBatches(automatedList.id, entities, deleteBatchSize);
                        }
                        winston.error(`Trying to remove all items from WL ${tagType} tag (${tagId})`)
                    })
            }
            winston.info(`${tagType} ${tag.name} : No automated list found. Merging existing whitelists into new list`);
            return this.mergeWhitelistsAndAttach(tag.data, tagType, entities);
        })
};

ListsApi.prototype.removeItemsFromBlacklist = function (tag, entities, tagType) {
    winston.info(`${tagType} ${tag.data.name} has a blacklist, trying to remove ${entities.length} items`);
    return this.getAutomatedList(tag.data[this.listIdsKey])
        .then(automatedList => {
            if (automatedList) {
                return this.removeFromListInBatches(automatedList.id, entities, deleteBatchSize);
            }
            winston.error(`${tagType} ${tag.name} : No automated list found`);
        })
};

ListsApi.prototype.mergeWhitelistsAndAttach = function (tag, tagType, itemsToBlock) {
    winston.info(`${tagType} ${tag.name} : Original ${this.listType} lists: ${tag[this.listIdsKey]}`);
    return this.getAllItemsFromLists(tag[this.listIdsKey])
        .then(wlItems=>{
            const wlFinal = wlItems.filter(item => !itemsToBlock.includes(item));
            if (wlFinal.length === 0) {
                winston.error(`Trying to remove all ${this.listType} from ${tagType} ${tag.name} (${tag.id})`);
                return;
            }
            tag[this.listIdsKey] = [];
            return this.createAndAttachList(tag, tagType, wlFinal, WHITE_LIST);
        });
};

ListsApi.prototype.addItemsToBlacklist = function (tag, items, tagType) {
    winston.info(`${tagType} ${tag.data.name} has a blacklist, trying to add ${items.length} items`);
    return this.getAutomatedList(tag.data[this.listIdsKey])
        .then(automatedList => {
            if (automatedList) {
                return this.addToListInBatches(automatedList.id, items);
            }
            return this.createAndAttachList(tag.data, tagType, items, BLACK_LIST)
        })
};

ListsApi.prototype.addItemsToWhitelist = function (tagId, items, tagType) {
    return this.api.getTag(tagId, tagType)
        .then(tag=>{
            return this.addItemsToWhiteListWithTag(tag, items);
        })
};

ListsApi.prototype.addItemsToWhiteListWithTag = function (tag, items) {
    return this.getAutomatedList(tag.data[this.listIdsKey])
        .then(automatedList => {
            if (automatedList) {
                return this.addToListInBatches(automatedList.id, items);
            }
            throw `This tag should have an automated list`;
        })
};

ListsApi.prototype.filterAndMap = function (items) {
    return items.filter(row=>row.excluded === 1).map(row=>row[this.rowKey]).filter(row=>row !== 'unknown');
};

ListsApi.prototype.checkIfWhiteList = function (tagData) {
    return tagData[this.targetingKey] === WHITE_LIST;
};

ListsApi.prototype.unblockItemsFromTag = function (tagId, tagType, items) {
    if (items.length === 0) {
        return Promise.resolve();
    }
    return this.api.getTag(tagId, tagType)
        .then(tag=>{
            if (tag.data[this.targetingKey] === BLACK_LIST) {
                return this.removeItemsFromBlacklist(tag, items, tagType);
            }
            if (tag.data[this.targetingKey] === WHITE_LIST) {
                return this.addItemsToWhiteListWithTag(tag, items, tagType, tagId);
            }
            if (tag.data[this.targetingKey] === TARGETING_ALL) {
                winston.error(`This tag (${tagId}) should have ${this.listType} targeting`);
            }
        })
        .then(()=>this.api.getTag(tagId, tagType))
};

ListsApi.prototype.blockItemsFromTag = function (tagId, tagType, items) {
    if (items.length === 0) {
        return Promise.resolve();
    }
    return this.api.getTag(tagId, tagType)
        .then(tag=>{
            if (tag.data[this.targetingKey] === BLACK_LIST) {
                return this.addItemsToBlacklist(tag, items, tagType);
            }
            if (tag.data[this.targetingKey] === WHITE_LIST) {
                return this.removeItemsFromWhitelist(tag, items, tagType, tagId);
            }
            if (tag.data[this.targetingKey] === TARGETING_ALL) {
                return this.createAndAttachList(tag.data, tagType, items, BLACK_LIST)
            }
        })
        .then(()=>this.api.getTag(tagId, tagType))
};
