const _ = require('lodash');
const axios = require("axios");
const Promise = require("bluebird");
const rp = require("request-promise");
const winston = require('winston');
const dateFormat = require('dateformat');
const util = require('util');
const ListsApi = require('./api_lists');

const TAG_TYPE_DEMAND = 'demand';
const TAG_TYPE_SUPPLY = 'supply';
const TARGETING_ALL = 'All';
const TARGETING_MIX = 'MIX';
const TARGETING_SP = 'SP';
const TARGETING_MP = 'MP';
const TARGETING_LP = 'LP';
const EXCLUDE = 'Exclude';
const INCLUDE = 'Include';
const WHITE_LIST = 'Whitelist';
const maxRetries = 3;
const baseUrl = `https://video.springserve.com`;

const requestWithRetry = {
    post: function postWithRetry(url, payload, headers) {
        return axios.post(url, payload, headers)
            .catch(err => {

            })
    },
    put: function postWithRetry(url, payload, headers) {
        return axios.put(url, payload, headers)
            .catch(err => {

            })
    },
};

class Api {
    constructor(user, pass) {
        this.user = user;
        this.pass = pass;
        this._listManagers = {
            domains: new ListsApi(this, 'domains'),
            app_bundles: new ListsApi(this, 'app_bundles')
        };
        this.requestWithRetry = requestWithRetry;
        this.platformShortName = {
            desktop: 'DT',
            in_app: 'IA',
            mobile_web: 'MW',
            dc: 'DC',
        };
    }

    get token() {
        return this._token;
    }

    get listManagers() {
        return this._listManagers;
    }

    set token(value) {
        this._token = value;
    }

    login() {
        return _login(this.user, this.pass)
            .then(token=>{
                this.token = token;
                return token;
            })
    }
    getSupplyReport() {
        return _getSupplyReport(this.token);
    }
    getSupplyReportLast7Days() {
        return _getSupplyReportLast7Days(this.token);
    }
    getDemandReport() {
        return _getDemandReport(this.token);
    }
    getSupplyDomainReport() {
        return _getSupplyDomainReport(this.token);
    }
    getSupplyWlReport() {
        return _getSupplyWlReport(this.token);
    }
    getDemandsDomainReport() {
        return _getDemandsDomainReport(this.token);
    }
    getDemandsGeoReport() {
        return _getDemandsGeoReport(this.token);
    }
    getTag(tagId, tagType) {
        return _getTag(this.token, tagId, tagType);
    }

    getDemandTag(tagId) {
        return _getTag(this.token, tagId, 'demand');
    }

    editTag(tagId, tagType, payload) {
        return _editTag(this.token, tagId, tagType, payload);
    }

    editDemandTag(tagId, payload) {
        return _editTag(this.token, tagId, 'demand', payload);
    }

    blockGeosFromDemand(demandId, geos) {
        return _blockGeosFromDemand(this.token, demandId, geos);
    }

    unblockGeosFromDemand(demandId, geos) {
        return _unblockGeosFromDemand(this.token, demandId, geos);
    }

    geosFilterAndMap(demandData) {
        return demandData.filter(row => row.excluded === 1).map(row => row.country_code);
    }

    getTagByName(name, tagType, params = []) {
        let url = `${baseUrl}/api/v0/${tagType}_tags?name=${encodeURIComponent(name)}`;
        params.forEach(p=> {url += `&${p}`});
        return axios.get(url, {headers: {Authorization: `${this.token}`}});
    }

    getAllActiveTags(tagType) {
        return this.getTagByName('', tagType, ['per=1000', 'active=true']);
    }

    getFloatingDuplicates(tagType) {
        return this.getTagByName('%(copy)%', tagType, ['active=true'])
    }

    deactivateTag(tagType, tagId) {
        winston.info(`Deactivating tag ${tagId}`);
        return this.editTag(tagId, tagType, {active:false})
    }

    deactivateTags(tagType, tagIds) {
        if (tagIds.length === 0) {
            return;
        }
        return this.deactivateTag(tagType, tagIds.pop())
            .then(()=> this.deactivateTags(tagType, tagIds));
    }

    closeFloatingDuplicates(tagType) {
        return this.getFloatingDuplicates(tagType)
            .then(res=>{
                winston.info(`${res.data.length} floating duplicates found`);
                return this.deactivateTags(tagType, res.data.map(t=>t.id))
            })
    }

    createOpposite(originalTagName, demandId, listType, items) {
        const newTagName = `opposite_tag_${originalTagName}_${demandId}_${new Date().toJSON().slice(0,10).replace(/-/g,'/')}`;
        const payload = {
            name: newTagName,
            budget_period: 'day',
            budget_pacing: 'even',
            budget_value: '100000',
            budget_metric: 'requests'
        };
        return this.duplicateTag(originalTagName, demandId, TAG_TYPE_DEMAND)
            .then(async newDemandTag => {
                if (newDemandTag === undefined) {
                    throw `Failed to duplicate tag`
                }
                winston.info(`Tag ${originalTagName} duplicated. New tag id: ${newDemandTag.id}`);
                if (newDemandTag.direct_connect !== true) {
                    payload.demand_partner_id = 32630;
                }

                const newAutomatedList = await this.listManagers[listType].createAutomatedList(newTagName);
                await this.listManagers[listType].addToListInBatches(newAutomatedList.data.id, items);
                payload[this.listManagers[listType].listIdsKey] = [newAutomatedList.data.id];
                payload[this.listManagers[listType].targetingKey] = WHITE_LIST;

                return this.editDemandTag(newDemandTag.id, payload)
            })
            .then(res => res.data);
    }

    blockDomainsFromDemand(tagId, domains) {
        return this.listManagers.domains.blockItemsFromTag(tagId, TAG_TYPE_DEMAND, domains)
    }

    convertToDomainWhitelist(tagId, tagType, domains) {
        winston.info(`Converting ${tagType} ${tagId} to whitelist`);
        return this.getTag(tagId, tagType)
            .then(tag=>{
                if (tag.data.domain_targeting === 'White List') {
                    throw `Tag ${tagId} is already a whitelist, not performing any change`;
                }
                return this.listManagers.domains.createAutomatedList(tag.data.name)
            })
            .then(automatedList=>{
                winston.info(`Created a new list for ${tagType} ${tagId} : ${automatedList.data.id}`);
                return this.listManagers.domains.addToListInBatches(automatedList.data.id, domains)
                    .then(()=>{
                        const payload = {
                            domain_targeting: 'White List',
                            domain_list_ids: [automatedList.data.id]
                        };
                        return this.editTag(tagId, tagType, payload);
                    })
            })
            .catch(err=>{
                winston.info(util.inspect(err));
            })
    }

    async createSupplyWhitelistTag(originalTagId, partner, newRate, allDemands, geos, domains, appBundles) {
        const tag = await this.getTag(originalTagId, TAG_TYPE_SUPPLY);
        const newSupplyTag = await this.duplicateTag(tag.data.name, originalTagId, TAG_TYPE_SUPPLY);

        if (newSupplyTag === undefined) {
            throw `Duplication of supply tag ${originalTagId} failed`;
        }
        winston.info(`New supply tag created: ${newSupplyTag.id}`);

        const roundedRate = Math.round(newRate * 100) / 100;
        const shortEnv = this.platformShortName[newSupplyTag.environment];
        const playerSize = getPlayerSize(newSupplyTag);
        const geoTargeting = geos.length > 2 ? 'WW' : geos.join('_');
        const domainTargeting = (domains && domains.length === 1)
            ? domains[0] : 'WL';
        const dateString = dateFormat(new Date(), 'ddmm');
        const payload = {
            name: `[V]_${partner}_$${roundedRate}_${shortEnv}_${playerSize}_${geoTargeting}_${domainTargeting}_${dateString}`,
            rate: newRate,
            country_targeting: INCLUDE,
            country_codes: geos,
            demand_tag_priorities: newSupplyTag.demand_tag_priorities,
        };

        newSupplyTag.name = payload.name;
        if (domains && domains.length > 0) {
            await this.listManagers.domains.createAndReplaceList(newSupplyTag, TAG_TYPE_SUPPLY, domains, WHITE_LIST)
        }
        if (appBundles && appBundles.length > 0) {
            await this.listManagers.app_bundles.createAndReplaceList(newSupplyTag, TAG_TYPE_SUPPLY, appBundles, WHITE_LIST)
        }

        const demandIdsToRemove = [];
        newSupplyTag.demand_tag_priorities.forEach(attachedDemand => {
            const demandData = allDemands.find(d => d.id === attachedDemand.demand_tag_id);

            if (demandData === undefined) {
                demandIdsToRemove.push(attachedDemand.demand_tag_id);
                return;
            }

            if (Number(demandData.rate) - newRate < 0.2 || this.checkIfTagIsUnique(demandData.name)) { // Removing this demand from the new WL supply
                demandIdsToRemove.push(demandData.id);
            }
        });
        _.remove(payload.demand_tag_priorities, d => demandIdsToRemove.includes(d.demand_tag_id));

        allDemands.forEach(demand => {
            if (payload.demand_tag_priorities.map(d => d.demand_tag_id).includes(demand.id)) {
                return;
            }
            if (this.checkIfTagIsUnique(demand.name)) {
                return;
            }
            if (Number(demand.rate) > 0.5 + newRate && demand.environment === newSupplyTag.environment) {
                payload.demand_tag_priorities.push({demand_tag_id: demand.id})
            }
        });
        return this.editTag(newSupplyTag.id, TAG_TYPE_SUPPLY, payload);
    }

    checkIfTagIsUnique(tagName) {
        return ['UNQ', 'unique'].some(s => tagName.includes(s))
    }

    duplicateTag(originalTagName, tagId, tagType) {
        const options = {
            uri: `${baseUrl}/api/v0/${tagType}_tags/${tagId}/duplicate`,
            headers: {
                Authorization: `${this.token}`
            }
        };
        return rp(options)
            .then(res=>{
                const parsed = JSON.parse(res);
                if (parsed === undefined) {
                    return this.getUnresponsiveDuplicate(originalTagName, tagType);
                }
                return parsed;
            })
            .catch(err=>{
                if ([502, 504].includes(err.statusCode)) {
                    return this.getUnresponsiveDuplicate(originalTagName, tagType);
                }
                throw err;
            });
    }

    getUnresponsiveDuplicate(originalTagName, tagType, retry = 0) {
        return this.getTagByName(`${originalTagName} (copy)`, tagType, ['active=true'])
            .then(res=>{
                const mostRecentTagFound = res.data.slice(-1)[0];
                if (mostRecentTagFound === undefined) {
                    if (retry >= 15) {
                        throw `Duplicated tag could not be found`;
                    }
                    winston.info(`Duplicated tag not found, waiting 15 seconds. Retry #${retry}`);
                    return Promise.delay(15000)
                        .then(()=>{
                            return this.getUnresponsiveDuplicate(originalTagName, tagType, retry + 1)
                        })
                }
                return mostRecentTagFound
            })
    }

    supplyCountryTargetingIsSimilar(countries, tagData) {
        return _.isEqual(countries, tagData.country_codes) && tagData.country_targeting === INCLUDE;
    }
}

function _login(user, pass) {
    const payload = {
        "email": user,
        "password": pass
    };
    return axios.post(`${baseUrl}/api/v0/auth`, payload)
        .then(res => {
            return res.data.token;
        })
}

function _getReport(token, params, pageNumber = 1, simlReqs = 1, rows = [], retry = 0) {
    params.page = pageNumber;
    return axios.post(`${baseUrl}/api/v0/report`, params, {headers: {Authorization: `${token}`}})
        .then(res=>{
            winston.info(`Rows batch #${pageNumber} received - ${rows.length} - ${rows.length + res.data.length}`);
            if (res.data.length === 0) {
                return rows;
            }
            return _getReport(token, params, pageNumber + simlReqs, simlReqs, rows.concat(res.data))
        })
        .catch(err=>{
            winston.info(util.inspect(err));
            if (retry < maxRetries) {
                winston.info(`Retrying request`);
                return _getReport(token, params, pageNumber, simlReqs, rows, retry + 1);
            }
        })
}

async function _getReportPagesSiml(token, params, parallelReqs) {
    const promises = Array.from(Array(parallelReqs).keys()).map(reqNum => {
        return _getReport(token, Object.assign({}, params), reqNum, parallelReqs)
    });
    const responses = await Promise.all(promises);

    return _.flatten(responses);
}

function _getSupplyWlReport(token) {
    const endDate = new Date();
    const startDate = new Date();
    startDate.setDate(startDate.getDate() - 1);

    const reportParams = {
        "start_date": `${dateFormat(startDate, "yyyy-mm-dd hh:MM:ss")}`, // "2018-06-13 07:00:00",
        "end_date": `${dateFormat(endDate, "yyyy-mm-dd hh:MM:ss")}`,
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["supply_tag_id", "declared_domain", "app_bundle", "supply_partner_id", "country", "environment"]
    };
    return _getReportPagesSiml(token, reportParams, 10);
}

function _getSupplyReport(token) {
    const reportParams = {
        "date_range": "Today",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["supply_tag_id"]
    };
    return _getReport(token, reportParams);
}

function _getSupplyReportLast7Days(token) {
    const reportParams = {
        "date_range": "Last 7 Days",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["supply_tag_id", "supply_partner_id"]
    };
    return _getReport(token, reportParams);
}

function _getDemandReport(token) {
    const reportParams = {
        "date_range": "Today",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["demand_tag_id"]
    };
    return _getReport(token, reportParams);
}

function _getSupplyDomainReport(token) {
    const reportParams = {
        "date_range": "Yesterday",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["supply_tag_id", "declared_domain", "app_bundle", "supply_partner_id", "environment"]
    };
    return _getReport(token, reportParams);
}

function _getDemandsDomainReport(token) {
    const reportParams = {
        "date_range": "Yesterday",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["demand_tag_id", "app_bundle", "declared_domain", "demand_partner_id", "environment"]
    };
    return _getReport(token, reportParams);
}

function _getDemandsGeoReport(token) {
    const reportParams = {
        "date_range": "Yesterday",
        "interval": "cumulative",
        "timezone": "UTC",
        "dimensions": ["demand_tag_id", "country", "demand_partner_id"]
    };
    return _getReport(token, reportParams);
}

function _getTag(token, tagId, tagType) {
    return axios.get(`${baseUrl}/api/v0/${tagType}_tags/${tagId}`, {headers: {Authorization: `${token}`}})
}

function _editTag(token, tagId, tagType, payload) {
    return axios.put(`${baseUrl}/api/v0/${tagType}_tags/${tagId}`, payload, {headers: {Authorization: `${token}`}})
}

function _getDemandTag(token, demandId) {
    return _getTag(token, demandId, TAG_TYPE_DEMAND);
}

function _editDemandTag(token, demandId, payload) {
    return _editTag(token, demandId, TAG_TYPE_DEMAND, payload);
}

function _blockGeosFromDemand(token, demandId, geos) {
    if (geos.length === 0) {
        return Promise.resolve();
    }
    return _getDemandTag(token, demandId)
        .then(demandTag=>{
            if (demandTag.data.country_targeting === EXCLUDE) {
                return _editDemandTag(token, demandId, {country_codes: _.union(geos, demandTag.data.country_codes)})
            }
            if (demandTag.data.country_targeting === INCLUDE) {
                _.remove(demandTag.data.country_codes, country=>geos.includes(country));
                if (demandTag.data.country_codes.length === 0) {
                    winston.error(`Trying to remove all geos from demand ${demandId}`);
                    return;
                }
                return _editDemandTag(token, demandId, {country_codes: demandTag.data.country_codes});
            }
            return _editDemandTag(token, demandId, {country_targeting: EXCLUDE, country_codes: geos});
        })
}

function _unblockGeosFromDemand(token, demandId, geos) {
    if (geos.length === 0) {
        return Promise.resolve();
    }
    return _getDemandTag(token, demandId)
        .then(demandTag=>{
            if (demandTag.data.country_targeting === EXCLUDE) {
                _.remove(demandTag.data.country_codes, country=>geos.includes(country));
                return _editDemandTag(token, demandId, {country_codes: demandTag.data.country_codes});
            }
            if (demandTag.data.country_targeting === INCLUDE) {
                return _editDemandTag(token, demandId, {country_codes: _.union(geos, demandTag.data.country_codes)})

            }
            winston.error(`No geo filter found for ${demandId}`);
            return Promise.resolve();
        })
}

function getPlayerSize(tagData) {
    if (tagData.player_size_targeting === TARGETING_ALL) {
        return TARGETING_MIX;
    }
    const playerSizes = _.keyBy(tagData.allowed_player_sizes);

    const hasSmall = Boolean(playerSizes.xs || playerSizes.s);
    const hasMedium = Boolean(playerSizes.m);
    const hasLarge = Boolean(playerSizes.l || playerSizes.xl);

    if (!hasSmall && hasMedium && !hasLarge) {
        return TARGETING_MP;
    }
    if (hasSmall && !hasLarge) {
        return TARGETING_SP;
    }
    if (!hasSmall && hasLarge) {
        return TARGETING_LP;
    }
    return TARGETING_MIX
}

module.exports = Api;