const utils = require('./utils');
const Promise = require('bluebird');
const util = require('util');
const winston = require("winston");
const _ = require("lodash");
const storeReport = require('./store_report');
const config = require('../config/config');
const executeQueryConfig = require('./db');

const concurrency = 1;

module.exports = {
    getGeoTtl,
    setGeoTtl,
    getGeosToBringBack,
    blockGeos,
    bringBackGeosByTtl,
};


function bringBackGeosByTtl(api) {
    const geosToBringBackPerTag = getGeosToBringBack();
    winston.info(`Starting to bring back geos according to their ttl`);

    const promiseProducer = function () {
        if (_.isEmpty(geosToBringBackPerTag)) {
            return null;
        }

        const demandId = Object.keys(geosToBringBackPerTag)[0];
        winston.info(`Starting promise chain for ${demandId}`);
        const geos = geosToBringBackPerTag[demandId];
        delete geosToBringBackPerTag[demandId];

        return api.unblockGeosFromDemand(demandId, geos)
            .then((res) => {
                winston.info(`(${demandId}) - ${geos.length} geos unblocked`);
            })
            .catch(err => {
                winston.error(`Error handling ${demandId}: ${util.inspect(err)}`)
            })
    };

    return utils.fulfillPromisesSync(promiseProducer, concurrency);
}

function blockGeos(report, api, tableName, mapObjectFields) {
    const promiseProducer = function () {
        if (_.isEmpty(report)) {
            return null;
        }

        const demand = Object.keys(report)[0];
        winston.info(`Starting promise chain for ${demand}`);
        const demandData = report[demand];
        delete report[demand];

        const geosToBlock = api.geosFilterAndMap(demandData);

        return api.blockGeosFromDemand(demandData[0].id, geosToBlock)
            .then((res) => {
                winston.info(`(${demandData[0].id}) ${demand} - ${geosToBlock.length} geos blocked: ${geosToBlock}`);
                return storeReport(mapObjectFields(demandData), tableName);
            })
            .catch(err => {
                winston.error(`Error handling ${demand}: ${util.inspect(err)}`)
            })
    };

    return utils.fulfillPromisesSync(promiseProducer, concurrency);
}

function getGeoTtl(accountId) {
    return executeQueryConfig(`select * from geo_ttl where account=?`, [accountId])
        .then((res)=>{
            const ttl = {};
            res.forEach(row=>{
                ttl[row.tag_id] = ttl[row.tag_id] || {};
                ttl[row.tag_id][row.geo] = row;
            });
            config.ttl = ttl;
            return config.ttl;
        })
}

function setGeoTtl(accountId) {
    const cols = ['account', 'tag_id', 'geo', 'date', 'times_blocked'];
    const placeHolders = [];
    const queryParams = [];

    _.forOwn(config.ttl, (tagObject, tagId) => {
        _.forOwn(tagObject, (geoObject, countryCode) => {
            placeHolders.push(Array.from('?'.repeat(cols.length)).join(','));
            queryParams.push(accountId);
            queryParams.push(tagId);
            queryParams.push(countryCode);
            queryParams.push(geoObject.date);
            queryParams.push(geoObject.times_blocked);
        })
    });

    return executeQueryConfig(`
insert into geo_ttl 
(${cols.join(',')}) 
VALUES 
(${placeHolders.join('),\n(')}) 
ON DUPLICATE KEY UPDATE
account = account, 
tag_id = tag_id, 
geo = geo, 
date = VALUES(date), 
times_blocked = VALUES(times_blocked)
`, queryParams, false);
}

function getGeosToBringBack() {
    const geosToBringBackPerTag = {};
    const today = new Date();
    today.setHours(0,0,0,0);
    _.forOwn(config.ttl, (tagObject, tagId) => {
        const geosToBringBack = [];
        _.forOwn(tagObject, (geoObject, countryCode) => {
            if (geoObject.date.getTime() === today.getTime() && geoObject.times_blocked < 3) {
                geosToBringBack.push(countryCode);
            }
        });
        geosToBringBackPerTag[tagId] = geosToBringBack;
    });
    return geosToBringBackPerTag;
}
