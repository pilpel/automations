const _ = require('lodash');
const winston = require('winston');
const config = require('../config/config');
const utils = require('./utils');
const filtersFieldMap = {
    tag_name: 'supply_tag_name',
    tag_id: 'supply_tag_id',
    partner: 'partner',
    env: 'env'
};

module.exports = analyze;

function analyze(rows, filters) {
    const settings = config.automationSettings;
    const totalRequests = _.sumBy(rows, row => Number(row.requests));
    const totalImpressions = _.sumBy(rows, row => Number(row.impressions));
    const sumsBySupply = {};

    const filteredRows = utils.applyFilters(rows, filters, filtersFieldMap);
    const rowsBySupply = _.groupBy(filteredRows, 'supply_tag_name');

    const logArr = [];

    _.forOwn(rowsBySupply, (rowsOfSupply, supply)=>{
        sumsBySupply[supply] = {
            requests: _.sumBy(rowsOfSupply, row => Number(row.requests)),
            impressions: _.sumBy(rowsOfSupply, row => Number(row.impressions)),
        };

        // Step A
        if (sumsBySupply[supply] / totalRequests < settings.min_relative_req || rowsOfSupply.length <= 3) {

            // For development logging
            let infoTag;
            if (sumsBySupply[supply] / totalRequests < settings.min_relative_req) {
                infoTag = 'Low req for tag';
            }
            if (rowsOfSupply.length <= 3) {
                infoTag = 'low domain count';
            }
            rowsOfSupply.forEach(row=> {
                // For development logging
                row.req_rel_to_tag = sumsBySupply[supply].requests && row.requests / sumsBySupply[supply].requests;
                row.imps_rel_to_tag = sumsBySupply[supply].impressions && row.impressions / sumsBySupply[supply].impressions;
                row.timeout_rate = row.requests && (row.timeouts / row.requests) * 100;
                row.info = infoTag;
            });
            //

            return;
        }

        rowsOfSupply.forEach(row=>{
            row.req_rel_to_tag = sumsBySupply[supply].requests && row.requests / sumsBySupply[supply].requests;
            row.imps_rel_to_tag = sumsBySupply[supply].impressions && row.impressions / sumsBySupply[supply].impressions;
            row.timeout_rate = row.requests && (row.timeouts / row.requests) * 100;

            // Ignore unclassified domains/bundles
            if (row.declared_domain === 'unclassified') {
                return;
            }

            // Step B
            if (row.requests / sumsBySupply[supply].requests < settings.min_req_for_domain || row.requests < settings.min_req_amnt) {
                row.info = 'Low req for domain/bundle';
                return;
            }

            // Step C
            if ((row.timeouts > settings.min_to || row.missed_opportunities > settings.min_missed_opps)
                && row.impressions / sumsBySupply[supply].impressions < settings.min_relative_imps_condition_1
                && row.fill_rate < settings.min_fr) {

                row.excluded = 1;
                row.info = 'timeouts OR opps';
                return;
            }

            // Step D
            if (row.requests > settings.req_threshold && row.impressions === 0) {
                row.excluded = 1;
                row.info = '0 impressions';
                return;
            }

            // Step E
            if (row.req_rel_to_tag > settings.req_to_imps_ratio * row.imps_rel_to_tag && row.imps_rel_to_tag < settings.rel_imps_per_domain_threshold) {
                row.excluded = 1;
                row.info = 'fillrate';
                return;
            }
        });
    });

    winston.info(`Total supply tags per run: ${Object.keys(rowsBySupply).length}`);
    winston.info(`Total supply tags after filtering: ${Object.keys(rowsBySupply).length}`);

    return {rows: rowsBySupply, rowsForLogging: logArr};
}