const _ = require('lodash');
const winston = require('winston');
const config = require('./../config/config');
const utils = require('./utils');
const filtersFieldMap = {
    tag_name: 'demand_tag_name',
    tag_id: 'demand_tag_id',
    partner: 'partner',
    env: 'env'
};

module.exports = analyze;

function analyze(rows, filters) {
    const settings = config.automationSettings;
    const totalRequests = _.sumBy(rows, row => Number(row.requests));
    const totalImpressions = _.sumBy(rows, row => Number(row.impressions));
    const sumsByDemand = {};

    const filteredRows = utils.applyFilters(rows, filters, filtersFieldMap);
    const filteredRowsByDemand = _.groupBy(filteredRows, 'demand_tag_name');

    const logArr = [];

    _.forOwn(filteredRowsByDemand, (rowsOfDemand, demand)=> {
        sumsByDemand[demand] = {
            requests: _.sumBy(rowsOfDemand, row => Number(row.requests)),
            impressions: _.sumBy(rowsOfDemand, row => Number(row.impressions)),
        };
    });

    const highestReq = Math.max(...Object.values(sumsByDemand).map(e=>e.requests));

    _.forOwn(filteredRowsByDemand, (rowsOfDemand, demand)=>{
        // Step A
        if (sumsByDemand[demand].requests / totalRequests < settings.min_relative_req ||
            rowsOfDemand.length === 1 ||
            demand.includes('opposite_tag') ||
            sumsByDemand[demand].requests < 100 ||
            sumsByDemand[demand].requests < highestReq * 0.00001 ||
            (sumsByDemand[demand].requests > 5000 && sumsByDemand[demand].impressions === 0)) {

            let infoTag;
            if (sumsByDemand[demand] / totalRequests < settings.min_relative_req) {
                infoTag = 'Low req for tag';
            }
            if (rowsOfDemand.length === 1) {
                infoTag = 'one domain';
            }
            if (demand.includes('opposite_tag')) {
                infoTag = 'opposite tag';
            }
            // Step A2
            if (sumsByDemand[demand].requests < 100 || sumsByDemand[demand].requests < highestReq * 0.00001) {
                infoTag = 'Low req for tag (A2)';
            }
            // Step A3
            if (sumsByDemand[demand].requests > 5000 && sumsByDemand[demand].impressions === 0) {
                infoTag = 'High reqs and 0 imps, should close tag';
            }
            rowsOfDemand.forEach(row=> {
                // For development logging
                row.req_rel_to_tag = sumsByDemand[demand].requests && row.requests / sumsByDemand[demand].requests;
                row.imps_rel_to_tag = sumsByDemand[demand].impressions && row.impressions / sumsByDemand[demand].impressions;
                row.timeout_rate = row.requests && (row.timeouts / row.requests) * 100;
                row.info = infoTag;

                // Opposite logic
                if (demand.includes('opposite_tag')) {
                    if (row.imps_rel_to_tag > settings.backin_imps && row.fill_rate > settings.backin_fr) {
                        row.excluded = 1;
                        row.info = 'perform well on opposite tag';
                    }
                }
            });
            //

            return;
        }

        rowsOfDemand.forEach(row=>{
            row.req_rel_to_tag = sumsByDemand[demand].requests && row.requests / sumsByDemand[demand].requests;
            row.imps_rel_to_tag = sumsByDemand[demand].impressions && row.impressions / sumsByDemand[demand].impressions;
            row.timeout_rate = row.requests && (row.timeouts / row.requests) * 100;

            // Ignore unclassifed domains
            if (['unclassified', 'undefined'].includes(row.declared_domain)) {
                return;
            }

            // Step B
            if (row.requests / sumsByDemand[demand].requests < settings.min_req_for_domain || row.requests < 10000) {
                row.info = 'Low req for domain';
                return;
            }

            // Step C
            if (row.timeouts / row.requests > settings.min_to_rate && row.impressions / sumsByDemand[demand].impressions < settings.min_relative_imps_condition_1 && row.fill_rate < settings.min_fr) {
                row.excluded = 1;
                row.info = 'timeouts';
                return;
            }

            // Step D
            if (row.fill_rate < settings.min_fr && row.req_rel_to_tag > settings.req_to_imps_ratio * row.imps_rel_to_tag && row.imps_rel_to_tag < settings.imps_rel_to_tag ) {
                row.excluded = 1;
                row.info = 'fillrate';
                return;
            }
        });
    });

    return {rows: filteredRowsByDemand, rowsForLogging: logArr, sumsByDemand};
}