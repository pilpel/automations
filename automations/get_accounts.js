const executeQueryConfig = require('./db');
const config = require('../config/config');

module.exports = getAccounts;

function getAccounts(platformName, accountIds) {
    let query = `select * from automation_accounts where active=1`;
    const params = [];

    if (accountIds) {
        query += ` and id in (${new Array(accountIds.length).fill('?').join(',')})`;
        params.push(...accountIds);
    }

    if (platformName) {
        query += ` and platform=?`;
        params.push(platformName);
    }
    config.accounts = {};

    return executeQueryConfig(query, params)
        .then((rows) => {
            rows.forEach(row=>{
                config.accounts[row.id] = {};
                config.accounts[row.id].username = row.username;
                config.accounts[row.id].password = row.password;
                config.accounts[row.id].id = row.id;
                config.accounts[row.id].platform = row.platform;
            })
        });
}