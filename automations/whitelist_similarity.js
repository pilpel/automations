const utils = require('./utils');
const _ = require('lodash');
const winston = require("winston");
const Promise = require('bluebird');

module.exports = {
    filterExistingWhitelists,
};

function checkIfActiveInLastThreeDays(supplyId, supplyRows) {
    return supplyRows.some(row => row.requests > 0)
}

function checkSimilarityExceptDomains(api, rowsBySupplyId, wlData, existingSupplyTags) {
    const similarTags = Object.keys(rowsBySupplyId).filter(supplyId => rowsBySupplyId[supplyId][0].partner_name === wlData.partner);

    _.remove(similarTags, (supplyId) => {
        return existingSupplyTags[supplyId] === undefined ||
            !utils.checkNumberIsWithinRange(existingSupplyTags[supplyId].rate, wlData.price - 0.05, wlData.price + 0.05) ||
            !api.supplyCountryTargetingIsSimilar(wlData.countries, existingSupplyTags[supplyId]) ||
            !checkIfActiveInLastThreeDays(supplyId, rowsBySupplyId[supplyId])
    });
    return similarTags;
}

async function checkDomainSimilarity(api, similarTags, existingSupplyTags, wlData) {
    if (similarTags.length === 0) {
        return similarTags;
    }
    const promises = Object.keys(api.listManagers)
        .map(async listType => {
            if (wlData[listType] === undefined) {
                return
            }
            const listManager = api.listManagers[listType];
            _.remove(similarTags, (supplyId) => !listManager.checkIfWhiteList(existingSupplyTags[supplyId]));

            const domainsBySupply = await listManager.getTagsListsItems(_.pick(existingSupplyTags, similarTags));

            _.remove(similarTags, (supplyId) => !utils.isSubset(wlData[listType], domainsBySupply[supplyId]));
        });
    await Promise.all(promises);
    return similarTags;
}

async function filterExistingWhitelists(api, whitelists, existingSupplyTags, rowsBySupplyId) {
    const promises = Object.keys(whitelists)
        .map(async wlSupplyName=>{
            const wlData = whitelists[wlSupplyName];
            let similarTags = checkSimilarityExceptDomains(api, rowsBySupplyId, wlData, existingSupplyTags);

            if (similarTags.length === 0) {
                return
            }

            similarTags = await checkDomainSimilarity(api, similarTags, existingSupplyTags, wlData);

            if (similarTags.length > 0) {
                winston.info(`At least one similar tag found - Not opening new tag for ${wlSupplyName}, rate: ${wlData.price}, countries: ${wlData.countries}, partner: ${wlData.partner}, domains ${wlData.domains}, app bundles: ${wlData.app_bundles}. Similar Tags: ${similarTags}`);
                delete whitelists[wlSupplyName];
            }
        });
    await Promise.all(promises);
    return whitelists;
}
