const executeQuery = require('./db');
const _ = require('lodash');

function storeReport(rows, table, logQuery = false) {

    if (rows.length === 0) {
        return Promise.resolve();
    }

    const queryParams = [];
    const columns = Object.keys(rows[0]);

    const placeHolders = _.map(rows, (row)=>{
        columns.forEach(key=> queryParams.push(row[key]));
        return Array.from('?'.repeat(columns.length)).join(',');
    });

    const query = `
        insert into ${table} 
            (${columns.join(',')}) 
        VALUES 
            (${placeHolders.join('),\n (')})`;

    return executeQuery(query, queryParams, logQuery);
}

module.exports = storeReport;
