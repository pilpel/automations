"use strict";
const winston = require("winston");
const getSettings = require('./get_settings');
const Promise = require('bluebird');
const util = require('util');
const utils = require('./utils');
const geoBlacklistLogic = require('./demand_geo_logic');
const geoUtils = require('./geo_utils');
const _ = require("lodash");

const automationType = 'demand_geo';

function automationMain(account, platform, api, mapFieldsForDb, mapFieldsForLogic, tableName) {

    return Promise.all([
        getSettings(platform, automationType),
        utils.getFiltering(automationType, account.id),
        api.login(),
        geoUtils.getGeoTtl(account.id)
    ]).then(([, filters, token]) => {
        winston.info(`Token acquired: ${token}`);
        return api.getDemandsGeoReport()
            .then((report) => {
                return geoBlacklistLogic(mapFieldsForLogic(report), filters);
            })
        })
        .then((report) => {
            return geoUtils.blockGeos(report, api, tableName, mapFieldsForDb);
        })
        .then(()=>{
            return geoUtils.setGeoTtl(account.id);
        })
        .then(()=>{
            return geoUtils.bringBackGeosByTtl(api);
        })
        .then(()=>{
            winston.info(`Erasing old DB data`);
            return utils.clearDbEntriesOlderThan(tableName, 14);
        })
        .then(res => {
            winston.info(`${platform} demand geo automation finished`)
        })
        .catch(err => {
            winston.info(err);
            throw err;
        });
}

module.exports = automationMain;