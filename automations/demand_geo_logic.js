const _ = require('lodash');
const winston = require('winston');
const utils = require('./utils');
const config = require('./../config/config');
const filtersFieldMap = {
    tag_name: 'demandName',
    tag_id: 'id',
    partner: 'demandPartner',
};

module.exports = analyze;

function getTtlDays(timesBlocked, returnedYesterday, ttlStrike1, ttlStrike2, ttlMax) {
    if (returnedYesterday) {
        return timesBlocked === 1 ? ttlStrike2 : ttlMax;
    }
    return ttlStrike1;
}

function ttlTimesBlocked(returnedYesterday, currBlocks) {
    return returnedYesterday ? currBlocks + 1 : 1;
}

function updateTtlDataToConfig(row) {
    const ttlPath = `ttl.${row.id}.${row.geo}`;
    const ttlObj = _.get(config, ttlPath, {});
    const settings = config.automationSettings;
    const yesterday = new Date();
    yesterday.setHours(0, 0, 0, 0);
    yesterday.setDate(yesterday.getDate() - 1);
    const returnedYesterday = ttlObj.date && ttlObj.date.getTime() === yesterday.getTime();

    if (ttlObj.date > new Date()) {
        winston.error(`This geo (${row.geo}) should already be blocked on this demand (${row.id})`);
        return;
    }

    if (row.impressions === 0) {
        ttlObj.ttl = getTtlDays(ttlObj.times_blocked, returnedYesterday, settings.ttl_days_strike_1_no_imps, settings.ttl_days_strike_2_no_imps, settings.ttl_days_max)
    }
    else {
        ttlObj.ttl = getTtlDays(ttlObj.times_blocked, returnedYesterday, settings.ttl_days_strike_1_has_imps, settings.ttl_days_strike_2_has_imps, settings.ttl_days_max)
    }
    ttlObj.times_blocked = ttlTimesBlocked(returnedYesterday, ttlObj.times_blocked);

    ttlObj.date = new Date();
    ttlObj.date.setDate(ttlObj.date.getDate() + ttlObj.ttl);
    _.set(config, ttlPath, ttlObj);
}

function analyze(rows, filters) {
    const settings = config.automationSettings;
    const totalRequests = _.sumBy(rows, row => Number(row.requests));
    const rowsByDemand = _.groupBy(rows, 'demandName');
    const rowsByGeo = _.groupBy(rows, 'geo');
    const sumsByDemand = {};
    const sumsByGeo = {};
    let eligibleCount = 0;
    let demandsAffected = 0;

    _.forOwn(rowsByGeo, (rowsOfGeo, geo) => {
        sumsByGeo[geo] = {
            requests: _.sumBy(rowsOfGeo, row => Number(row.requests)),
            impressions: _.sumBy(rowsOfGeo, row => Number(row.impressions)),
        };
    });
    _.forOwn(rowsByDemand, (rowsOfDemand, demand) => {
        sumsByDemand[demand] = {
            requests: _.sumBy(rowsOfDemand, row => Number(row.requests)),
            impressions: _.sumBy(rowsOfDemand, row => Number(row.impressions)),
        };
    });

    const filteredRows = utils.applyFilters(rows, filters, filtersFieldMap);
    const filteredRowsByDemand = _.groupBy(filteredRows, 'demandName');

    _.forOwn(filteredRowsByDemand, (rowsOfDemand, demand) => {
        let affected = false;
        rowsOfDemand.forEach(row=> {
            row.fillRate = Number(row.requests) && (Number(row.impressions) / Number(row.requests));
            row.reqsRelToTag = (Number(row.requests) / sumsByDemand[demand].requests) || 0;
            row.impsRelToTag = (Number(row.impressions) / sumsByDemand[demand].impressions) || 0;
        });

        //Step D
        if ((sumsByDemand[demand].requests / totalRequests)< settings.demand_req_percent && sumsByDemand[demand].impressions === 0) {
            rowsOfDemand.forEach(row => {
                row.info = 'high requests, no impressions in all geos';
            });
            return;
        }

        // Step C
        if ((sumsByDemand[demand].requests / totalRequests) < settings.demand_req_percent) {
            rowsOfDemand.forEach(row => {
                row.info = 'tag not met threshold';
            });
            return;
        }
        eligibleCount++;

        rowsOfDemand.forEach(row => {

            // Step E
            if (row.requests > settings.geo_req_threshold) {

                if (row.geo === '') {
                    return;
                }
                //Step H
                if (Number(row.impressions) / sumsByGeo[row.geo].impressions > settings.geo_exclude_threshold) {
                    return;
                }

                // Step F
                if ((Number(row.impressions) / sumsByDemand[demand].impressions) / (Number(row.requests) / sumsByDemand[demand].requests) < settings.rel_imps_vs_rel_req && (Number(row.impressions) / sumsByDemand[demand].impressions) < settings.relative_imps_a) {
                    affected = true;
                    row.excluded = 1;
                    row.info = 'low performance by relative ratio';
                    updateTtlDataToConfig(row);
                    return;
                }

                //Step G
                if ((Number(row.impressions) / Number(row.requests) < settings.fillrate_threshold) && (Number(row.impressions) / sumsByDemand[demand].impressions < settings.relative_imps_b)) {
                    affected = true;
                    row.excluded = 1;
                    row.info = 'low performance by fillrate';
                    updateTtlDataToConfig(row);
                }
            }
        });
        if (affected) {
            demandsAffected++;
        }
    });

    winston.info(`Total demand tags per run: ${Object.keys(rowsByDemand).length}`);
    winston.info(`Total demand tags after filtering: ${Object.keys(filteredRowsByDemand).length}`);
    winston.info(`Total demand tags analyzed: ${eligibleCount}`);
    winston.info(`Total demand tags affected: ${demandsAffected}`);
    winston.info(`Total demand tags not affected: ${Object.keys(rowsByDemand).length - demandsAffected}`);

    return filteredRowsByDemand;
}

