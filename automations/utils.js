const executeQueryConfig = require('./db');
const config = require('../config/config');
const _ = require('lodash');
const PromisePool = require('es6-promise-pool');
const winston = require("winston");
const storeReport = require('./store_report');

const SATURDAY = 6;
const SUNDAY = 0;
const FRIDAY = 5;
const ISRAEL_WEEKEND = [SATURDAY, FRIDAY];
const WEEKEND = [SATURDAY, SUNDAY];

const supplyWlTagsTable = 'supply_wl_tags';

module.exports = {
    clearDbEntriesOlderThan,
    getFiltering,
    applyFilters,
    getOppositeTagsMapping,
    addOppositeToMapping,
    addTagToFilters,
    sleeper,
    getRandomArbitrary,
    fulfillPromisesSync,
    fulfillSinglePromisesSync,
    getNewWhitelists,
    getTodaysSupplyBlacklistData,
    isSubset,
    checkNumberIsWithinRange,
    isWeekendInIsrael,
    isWeekend,
    getDaysDifferenceBetweenDates,
    storeWl,
    getPartnerEmails,
    capitalizeFirstLetter,
    getAlreadyOpenedWlPartners,
    prepForDb,
    removeBadChars,
};

function removeBadChars(str) {
    // return str.replace(/[\u0800-\uFFFF]/g, '');
    return str.replace(/([\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2580-\u27BF]|\uD83E[\uDD10-\uDDFF])/g, '');
}

function prepForDb(domainStr) {
    if ([null, undefined].includes(domainStr)) {
        return null
    }
    return domainStr.slice(0,100)
}

function getAlreadyOpenedWlPartners(accountId) {
    const dateObj = new Date();
    dateObj.setHours(0,0,0,0);
    dateObj.setDate(dateObj.getDate() - dateObj.getDay());
    const query = `select distinct partner from supply_wl_tags where account = ? and created_at > ?`;
    return executeQueryConfig(query, [accountId, dateObj])
        .then(res=>{
            return res.map(row => row.partner)
        })
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getPartnerEmails() {
    return executeQueryConfig(`SELECT * FROM partners;`)
        .then(res=>{
            const partnerEmails = {};
            res.forEach(row=>{
                partnerEmails[row.name] = row;
                partnerEmails[row.pub] = row;
            });
            return partnerEmails;
        })
}

function storeWl(accountId, platform, partnerName, originalTagId, tagId, tagName, price, geos, domains, appBundles, margin, environment) {
    const dataObject = {
        account: accountId,
        partner: partnerName,
        platform,
        original_tag_id: originalTagId,
        tag_id: tagId,
        tag_name: tagName,
        price,
        geos: JSON.stringify(geos),
        domains: JSON.stringify(domains),
        app_bundles: JSON.stringify(appBundles),
        margin,
        environment,
    };
    return storeReport([dataObject], supplyWlTagsTable, true)
}

function getDaysDifferenceBetweenDates(date1, date2) {
    const timeDiff = Math.abs(date2.getTime() - date1.getTime());
    return Math.ceil(timeDiff / (1000 * 3600 * 24));
}

function isWeekendInIsrael() {
    const today = new Date();
    return ISRAEL_WEEKEND.includes(today.getDay())
}

function isWeekend() {
    const today = new Date();
    return WEEKEND.includes(today.getDay())
}

function isSubset (arr1, arr2) {
    return arr1.every(val => arr2.indexOf(val) >= 0)
}

function checkNumberIsWithinRange(num, lowerBound, upperBound) {
    return num > lowerBound && num < upperBound
}

function getTodaysSupplyBlacklistData(blackListTableName) {
    return executeQueryConfig(`SELECT * FROM ${blackListTableName} WHERE created_at >= CURDATE();`)
}

function getNewWhitelists(accountId) {
    let query = `SELECT * FROM supply_wl_tags WHERE created_at >= CURDATE()`;
    if (accountId) {
        query += 'and account = ?'
    }
    return executeQueryConfig(query, accountId);
}

function clearDbEntriesOlderThan(tableName, lookBackWindow) {
    return executeQueryConfig(`delete from ${tableName} where created_at < DATE_SUB(CURDATE(), INTERVAL ? day)`, [lookBackWindow]);
}

function getFiltering(automationType, accountId) {
    return executeQueryConfig(`select * from automation_filtering where account=? and automation_type=?`, [accountId, automationType])
}

function applyFilters(rows, filters, fieldMap) {
    if (filters === undefined || filters.length === 0) {
        return rows;
    }
    filters = _.groupBy(filters, 'filter_type');

    return rows.filter(rowObj=>checkRowAgainstFilters(rowObj, filters, fieldMap));
}

function checkRowAgainstFilters(rowObj, filters, fieldMap) {
    let includeRow = true;

    if (filters.include) {
        includeRow = filters.include.some(filter => checkConditions(rowObj, filter, fieldMap, ops.equals, ops.and))
    }

    if (filters.exclude && includeRow) {
        includeRow = filters.exclude.every(filter => checkConditions(rowObj, filter, fieldMap, ops.notEquals, ops.or))
    }

    return includeRow;
}

function checkConditions(rowObj, filter, fieldMap, valueCompareOp, conditionCompareOp) {
    const rowValue = rowObj[fieldMap[filter.filter_field]];
    const condition1 = valueCompareOp(rowValue, filter.value);
    if (filter.filter_field_2) {
        const rowValue2 = rowObj[fieldMap[filter.filter_field_2]];
        const condition2 = valueCompareOp(rowValue2, filter.value_2);
        return conditionCompareOp(condition1, condition2);
    }
    return condition1;
}

const ops = {
    equals: (value1, value2) => value1 === value2,
    notEquals: (value1, value2) => value1 !== value2,
    or: (value1, value2) => value1 || value2,
    and: (value1, value2) => value1 && value2,
};

function fulfillPromisesSync(promiseProducer, concurrency) {
    const pool = new PromisePool(promiseProducer, concurrency);
    const poolPromise = pool.start();

    return poolPromise.then(function () {
        winston.info('All promises fulfilled');
    }, function (error) {
        winston.info('Some promise rejected: ' + error);
        throw error;
    });
}

function fulfillSinglePromisesSync(promiseProducer) {
    return fulfillPromisesSync(promiseProducer, 1)
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function addOppositeToMapping(accountId, originalTagName, originalTagId, oppositeTagId) {
    return executeQueryConfig(`insert into opposite_tags (account,original_tag_id,original_tag_name,opposite_tag_id) values (?,?,?,?)`, [accountId, originalTagId, originalTagName, oppositeTagId])
}

function addTagToFilters(accountId, automationType, tagName) {
    return executeQueryConfig(`insert into automation_filtering (account,automation_type,filter_type,name_in_platform) values (?,?,?,?)`, [accountId, automationType, 'include', tagName])
}

function getOppositeTagsMapping(accountId) {
    return executeQueryConfig(`select * from opposite_tags where account=?`, [accountId])
        .then(res=>{
            const oppositesMap = {reverseMapping: {}};
            res.forEach(row=>{
                oppositesMap[row.original_tag_id] = row.opposite_tag_id;
                oppositesMap.reverseMapping[row.opposite_tag_id] = row.original_tag_id;
            });
            return oppositesMap;
        })
}

function sleeper(ms) {
    return function(x) {
        return new Promise(resolve => setTimeout(() => resolve(x), ms));
    };
}
