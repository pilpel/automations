const _ = require('lodash');
const dateFormat = require('dateformat');
const utils = require('./../utils');

function introMessage(platformName) {
    return `
Dear partner,

Well done on the recent results on our ${platformName} tags!
`;
}

const msgByMarginType = {
    high: `
Please find below new WL tags with the best performing domains and geos and new higher competitive rates. Please do your best to increase amount of traffic. WL is below.
`,
    medium: `
Here are some additional tags for great performing tags where we changed the demand to better match your inventory. 
`,
    low: `
In addition, we have a few good performing sites and geos that performs much better, and at a much higher scale, at a bit lower rates. Please send the traffic through the new tags and you would see much better results! :)
`};
const blacklistIsAttachedMessage = `
Please find attached lists of domains/app bundles that should be blacklisted from the tag ID found in the file name.
`;

function getFollowUpEmail(inactiveTags) {
    const inactivesByPlatform = _.groupBy(inactiveTags, 'platform');

    let message = `Dear partner,
`;

    Object.keys(inactivesByPlatform).forEach(platformName => {
        message += `
On ${dateFormat(inactivesByPlatform[platformName][0].created_at, "fullDate")} you should have received the following WL tag(s) from ${utils.capitalizeFirstLetter(platformName)} - ${inactivesByPlatform[platformName].map(tagObj => tagObj.tag_id).join(', ')}
`
    });

    message += `
The tags have been active on our end for about ${utils.getDaysDifferenceBetweenDates(new Date(), inactiveTags[0].created_at)} days and with no activity. 
Did you get a chance to implement it? If so, please double check the targeting. 

Thank you,
Alex
`;
    return message;
}

function getSupplyMailText(partnersWlTags, platformName, partnerEmail) {
    let message = ``;

    message += `Partner Email: ${partnerEmail ? partnerEmail.email : 'not specified in `partners` table'}\n`;

    const wlsByMargin = _.groupBy(partnersWlTags, 'margin');

    message += introMessage(utils.capitalizeFirstLetter(platformName));

    function addMarginTypeToMessage(marginType) {
        if (wlsByMargin[marginType]) {
            message += msgByMarginType[marginType];
            wlsByMargin[marginType].forEach(wl=>{
                message += `Name: ${wl.tag_name}\n`;
                message += `Tag: ${exportedTagByEnv(platformName, wl.environment, wl.tag_id)}\n`;
                message += `Environment: ${envToPrettyEnv(wl.environment)}\n`;
                message += `Rate: ${wl.price}\n`;
                message += `Geos: ${JSON.parse(wl.geos).toString()}\n`;
                if (wl.domains) { message += `Domains: \n${prettyPrintItems(wl.domains)}\n` }
                if (wl.app_bundles) { message += `App-bundles: \n${prettyPrintItems(wl.app_bundles)}\n` }
                message += '\n';
            })
        }
    }
    Object.keys(msgByMarginType).forEach(marginType=>addMarginTypeToMessage(marginType));

    return message;
}

function prettyPrintItems(itemsStr) {
    const items = JSON.parse(itemsStr).slice(0,7).toString();
    return items.split(',').map(item => `&nbsp;&nbsp;&nbsp;&nbsp;${item}`).join('\n');
}

function exportedTagByEnv(platformName, env, tagId) {
    return {
        springserve: {
            desktop: `http://vid.springserve.com/vast/${tagId}?w={{WIDTH}}&h={{HEIGHT}}&url={{ENCODED_URL}}&cb={{CACHEBUSTER}}`,
            in_app: `http://vid.springserve.com/vast/${tagId}?w={{WIDTH}}&h={{HEIGHT}}&cb={{CACHEBUSTER}}&ip={{IP}}&ua={{USER_AGENT}}&lat={{LAT}}&lon={{LON}}&dnt={{DNT}}&app_bundle={{APP_BUNDLE}}&app_name={{APP_NAME}}&did={{DEVICE_ID}}&device_make={{DEVICE_MAKE}}&device_model={{DEVICE_MODEL}}`,
            mobile_web: `http://vid.springserve.com/vast/${tagId}?w={{WIDTH}}&h={{HEIGHT}}&url={{ENCODED_URL}}&cb={{CACHEBUSTER}}&ip={{IP}}&ua={{USER_AGENT}}&lat={{LAT}}&lon={{LON}}&dnt={{DNT}}`,
            dc: 'DC'
        },
        streamrail: {
            desktop: `https://ssp.streamrail.net/ssp/vpaid/56b9ef7f87cd190002000008/${tagId}?cb=[CB]&width=[WIDTH]&height=[HEIGHT]&dnt=[DO_NOT_TRACK]&sub_id=[SUB_ID]&ip=[IP]&ua=[UA]&user_consent=[USER_CONSENT]&gdpr=[GDPR]&page_url=[PAGE_URL]`,
            mobile_app: `https://ssp.streamrail.net/ssp/vpaid/56b9ef7f87cd190002000008/${tagId}?cb=[CB]&width=[WIDTH]&height=[HEIGHT]&dnt=[DO_NOT_TRACK]&sub_id=[SUB_ID]&app_name=[APP_NAME]&bundle_id=[BUNDLE_ID]&ifa=[IFA]&app_store_url=[APP_STORE_URL]&app_category=[APP_CATEGORY]&ip=[IP]&ua=[UA]&user_lat=[USER_LAT]&user_lon=[USER_LON]&user_consent=[USER_CONSENT]&gdpr=[GDPR]`,
            mobile_web: `https://ssp.streamrail.net/ssp/vpaid/56b9ef7f87cd190002000008/${tagId}?cb=[CB]&width=[WIDTH]&height=[HEIGHT]&dnt=[DO_NOT_TRACK]&sub_id=[SUB_ID]&ip=[IP]&ua=[UA]&user_lat=[USER_LAT]&user_lon=[USER_LON]&user_consent=[USER_CONSENT]&gdpr=[GDPR]&page_url=[PAGE_URL]`
        }
    }[platformName][env]
}

function envToPrettyEnv(env) {
    return {
        desktop: 'Desktop',
        in_app: 'In-App',
        mobile_web: 'Mobile Web',
        mobile_app: 'In-App',
        dc: 'Direct Connect',
    }[env]
}

const orenSignature = `<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style='margin-left:5.4pt;border-collapse:collapse'>
        <tr>
            <td width=208 rowspan=5 valign=top style='width:155.7pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal><b><span style='font-family:BoosterNextFY-Light;color:#CC0000'><img width=265
                                                                                                       height=192
                                                                                                       style='width:2.7604in;height:2.0in'
                                                                                                       id="Picture_x0020_8"
                                                                                                       src="cid:TIMlogo.png"
                                                                                                       alt="TIM_logo_4"></span></b>
                    <o:p></o:p>
                </p>
            </td>
            <td width=393 valign=top style='width:295.1pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal><b><span style='font-size:18.0pt;font-family:BoosterNextFY-Medium;color:#404040'><o:p>&nbsp;</o:p></span></b>
                </p>
                <p class=MsoNormal><b><span
                        style='font-size:18.0pt;font-family:BoosterNextFY-Medium'>Oren Liberman</span></b><b><span
                        style='font-size:18.0pt;color:#404040'><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=393 valign=top style='width:295.1pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal><b><span
                        style='font-size:12.0pt;font-family:BoosterNextFY-Light'>COO</span></b><b><span
                        style='font-size:12.0pt'><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=393 valign=top style='width:295.1pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal><b><span style='font-size:10.0pt;font-family:BoosterNextFY-Medium;color:#4D4D4D'>E-mail:</span></b><b><span
                        style='font-size:10.0pt;font-family:BoosterNextFY-Medium;color:#C00000'> </span></b><a
                        href="mailto:oren@thetimmedia.com">oren<span style='font-family:BoosterNextFY-Medium'>@thetimmedia.com</span></a><b><span
                        style='font-size:10.0pt'><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=393 valign=top style='width:295.1pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal><b><span
                        style='font-size:10.0pt;font-family:BoosterNextFY-Medium;color:#4D4D4D'>Tel: </span></b><b><span
                        style='font-size:10.0pt;font-family:BoosterNextFY-Light;color:#4D4D4D'>+972 – 54– 795 – 2528</span></b><b><span
                        style='font-size:10.0pt'><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr>
            <td width=393 valign=top style='width:295.1pt;padding:0in 5.4pt 0in 5.4pt'>
                <p class=MsoNormal align=right dir=RTL style='text-align:left;direction:rtl;unicode-bidi:embed'><b><span
                        dir=LTR style='font-size:10.0pt;font-family:BoosterNextFY-Medium;color:#4D4D4D'>Skype</span></b><b><span
                        dir=LTR
                        style='font-size:10.0pt;font-family:BoosterNextFY-Medium'>: oren.liberman</span></b><b><span
                        dir=LTR style='font-size:10.0pt;font-family:BoosterNextFY-Light;color:#4D4D4D'><o:p></o:p></span></b>
                </p>
                <p class=MsoNormal align=right dir=RTL style='text-align:left;direction:rtl;unicode-bidi:embed'><b><span
                        dir=LTR
                        style='font-size:10.0pt;font-family:BoosterNextFY-Medium;color:#4D4D4D'>LinkedIn: </span></b><b><span
                        dir=LTR style='font-size:10.0pt;font-family:BoosterNextFY-Light'><a
                        href="https://www.linkedin.com/in/oren-liberman-6267555b/">Oren Liberman</a><span
                        style='color:#4D4D4D'><o:p></o:p></span></span></b></p>
            </td>
        </tr>
    </table>`

module.exports = {
    getSupplyMailText,
    getFollowUpEmail,
    orenSignature,
};
