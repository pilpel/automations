const config = require('../../config/config');
const mailgun = require('mailgun-js')(config.mailer.auth);

module.exports = {
    sendMail,
    sendMailWithSignatureAndAttachment,
    generateAttachment,
};

function sendMail(toAddress, subject, text) {

    return new Promise((resolve, reject) => {

        const data = {
            from: config.mailer.fromMail,
            to: toAddress,
            subject: subject,
            text
        };

        mailgun.messages().send(data, function (error, body) {
            if (error) {
                reject(error);
            }
            resolve(body);
        });
    })
}

function generateAttachment(filename, fileBuffer) {
    return new mailgun.Attachment({data: fileBuffer, filename: filename});
}

function sendMailWithSignatureAndAttachment(toAddress, subject, text, signature, attachments) {

    return new Promise((resolve, reject) => {

        const data = {
            from: config.mailer.fromMail,
            to: toAddress,
            subject: subject,
            attachment: attachments,
            inline: 'automations/mail/TIMlogo.png',
            html: `
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">
<body>
${text.replace(new RegExp('\r?\n','g'), '<br>')}

${signature}
</body>
</html>
`,
        };

        mailgun.messages().send(data, function (error, body) {
            if (error) {
                reject(error);
            }
            resolve(body);
        });
    })
}

