const winston = require("winston");
const utils = require('./utils');
const util = require('util');
const _ = require('lodash');
const Promise = require('bluebird');
const mailFormats = require('./mail/mail_formats');
const mailgun = require('./mail/mailgun');

const automationType = 'partner_mail';

module.exports = start;

function partnerMailHandler(partnerName, wlData, partnerEmail, platform) {

    const emailMessage = mailFormats.getSupplyMailText(wlData, platform, partnerEmail);

    const recipients = ['automations@thetimmedia.com', 'alex.b@thetimmedia.com', 'aviv@thetimmedia.com'];
    // const recipients = ['daniel@thetimmedia.com']; // for dev

    winston.info(`Sending email to ${partnerName} : ${emailMessage}`);

    const fixedPartnerName = partnerEmail ? partnerEmail.name : partnerName;

    return mailgun.sendMailWithSignatureAndAttachment(recipients.join(','), `${fixedPartnerName} - Whitelist actions`, emailMessage, mailFormats.orenSignature)
        .then(() => {
            winston.info(`Email sent to ${partnerName}`);
        })
}

function start() {

    return Promise.all([
        utils.getNewWhitelists(),
        utils.getPartnerEmails(),
    ])
        .then(([wlData, partnerEmails]) => {

            const wlDataByPlatform = _.groupBy(wlData, 'platform');

            const promises = [];
            Object.keys(wlDataByPlatform).forEach(platformName => {

                const wlDataByPartner = _.groupBy(wlDataByPlatform[platformName], 'partner');

                Object.keys(wlDataByPartner).forEach(partnerName => {
                    promises.push(partnerMailHandler(partnerName, wlDataByPartner[partnerName], partnerEmails[partnerName], platformName))
                });
            });

            return Promise.all(promises);
        })
        .catch(err => {
            console.log(util.inspect(err));
        })
}