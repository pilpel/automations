const winston = require("winston");
const utils = require('./utils');
const Promise = require('bluebird');
const config = require('./../config/config');

const automationType = 'demand_domain';

module.exports = function blockFromListTypeAndHandleOpposites(api, listType, demandRows, demandId, oppositesMap, filters, account, demandName, sumsByDemand, tagType) {
    const itemsToBlock = api.listManagers[listType].filterAndMap(demandRows);
    if (itemsToBlock.length === 0) {
        return Promise.resolve();
    }
    return api.listManagers[listType].blockItemsFromTag(demandId, tagType, itemsToBlock)
        .then((tag) => {
            winston.info(`Demand (${demandId}) Finished blocking ${itemsToBlock.length} ${listType}`);

            if (oppositesMap[demandId]) {
                winston.info(`This tag has an opposite tag (${oppositesMap[demandId]}) - Adding items to opposite whitelist`);
                return api.listManagers[listType].addItemsToWhitelist(oppositesMap[demandId], itemsToBlock, tagType)
            }

            if (oppositesMap.reverseMapping[demandId]) {
                winston.info(`This tag is an opposite tag of ${oppositesMap.reverseMapping[demandId]} - Adding items back to original tag`);
                return api.listManagers[listType].unblockItemsFromTag(oppositesMap.reverseMapping[demandId], tagType, itemsToBlock);
            }
        }).then(()=>{
            if (config.automationSettings.open_new_opposites === 1 && oppositesMap[demandId] === undefined && oppositesMap.reverseMapping[demandId] === undefined && sumsByDemand[demandName].impressions > 100) {
                return createAndMapOppositeTag(api, demandId, filters, account, demandName, oppositesMap, listType, itemsToBlock)
            }
        })
};

function createAndMapOppositeTag(api, demandId, filters, account, demandName, oppositesMap, listType, itemsToBlock) {
    winston.info(`Opening opposite tag for ${demandId}`);
    return api.createOpposite(demandName, demandId, listType, itemsToBlock)
        .then(oppositeTagRes => {

            oppositesMap[demandId] = oppositeTagRes.id;
            oppositesMap.reverseMapping[oppositeTagRes.id] = demandId;

            const oppsPromises = [];
            if (filters.include) {
                oppsPromises.push(utils.addTagToFilters(account.id, automationType, oppositeTagRes.name));
            }
            oppsPromises.push(utils.addOppositeToMapping(account.id, demandName, demandId, oppositeTagRes.id));
            return Promise.all(oppsPromises);
        })
}

