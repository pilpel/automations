# V2V Automations
All automations start at the base dir in automations.js <br>
Config can be found at config/config.js

### Database
terser_automation (35.185.67.173)
- automation_accounts: Login data for platforms
- automation_filtering: Which tags to include in each automation
- automation_settings: The settings to be used for each automation type, for each account id.
- The following tables hold result data for their relevant jobs:
    - springserve_demand_domain
    - springserve_supply_domain
    - springserve_demand_geo
    - streamrail_demand_domain
    - streamrail_supply_domain
    - streamrail_demand_geo
- supply_wl_tags: Lists all opened whitelist tags. mail_follow_up_automation updates whether they have been active, and how many follow ups were sent for each tag.
- opposite_tags: Holds a mapping between each demand tag id and its opposite tag id.
- geo_ttl: When to bring back excluded geos to demand tags.

### Parameters
- platform_name:
    - If platform name isn't passed, defaults to all
- automation_type:
    - Mandatory value
    - Possible values: demand_geo, demand_domain, supply_domain, supply_wl, partner_mail, mail_follow_up
    - Values correspond to the 'automation_type' columns in the DB
- account_id:
    - If platform name isn't passed, defaults to all
    - corresponds to 'id' column in 'automation_accounts' table
    
For dev, parameters can also be set in automations.js. Make sure to remove before pushing to git.

### Rundeck
All automation jobs are scheduled to run using [The New Rundeck (c)](http://rundeck.thetimmedia.site:4440/project/V2V/jobs)<br>
See V2V Project -> 'External' dir <BR>
Rundeck holds logs for all jobs run by the automations

### Troubleshooting an automation job
- Run the program locally, setting the parameters to the relevant job that is causing the issue
- Refer to the API docs if the problem is related to one of the APIs
- Set a breakpoint at the relevant location and run debugger

### Specs docs
- [Demand domain blacklist](https://docs.google.com/document/d/1OdxVg68XI5Y7bF0K65vQGdysSLSRpG7UVPUA5GlDHUc/edit)
- [Geo blacklist](https://docs.google.com/document/d/1BPc8-2p43HlVrlfVC-sVnwBPYwp7PJX6NAv5lzg6x_k/edit?ts=5ab75514)
- [Supply domain blacklist](https://docs.google.com/document/d/1K0slIHSxBpYOefVo0h7YvNZwaM2s57LmFCc0yUmj0U4/edit?ts=5ab8efbe)
- [Supply whitelist](https://docs.google.com/document/d/1Y1007ZhKRTUSqagplPJLxA0bUmZWre7ozcj1yuZ-z7w/edit?ts=5acf3de4)

[Api documentation](docs/api.md)

